import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IClientBanque } from '../client-banque.model';
import { ClientBanqueService } from '../service/client-banque.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './client-banque-delete-dialog.component.html',
})
export class ClientBanqueDeleteDialogComponent {
  clientBanque?: IClientBanque;

  constructor(protected clientBanqueService: ClientBanqueService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.clientBanqueService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
