jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ClientBanqueService } from '../service/client-banque.service';

import { ClientBanqueDeleteDialogComponent } from './client-banque-delete-dialog.component';

describe('ClientBanque Management Delete Component', () => {
  let comp: ClientBanqueDeleteDialogComponent;
  let fixture: ComponentFixture<ClientBanqueDeleteDialogComponent>;
  let service: ClientBanqueService;
  let mockActiveModal: NgbActiveModal;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ClientBanqueDeleteDialogComponent],
      providers: [NgbActiveModal],
    })
      .overrideTemplate(ClientBanqueDeleteDialogComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ClientBanqueDeleteDialogComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ClientBanqueService);
    mockActiveModal = TestBed.inject(NgbActiveModal);
  });

  describe('confirmDelete', () => {
    it('Should call delete service on confirmDelete', inject(
      [],
      fakeAsync(() => {
        // GIVEN
        jest.spyOn(service, 'delete').mockReturnValue(of(new HttpResponse({ body: {} })));

        // WHEN
        comp.confirmDelete(123);
        tick();

        // THEN
        expect(service.delete).toHaveBeenCalledWith(123);
        expect(mockActiveModal.close).toHaveBeenCalledWith('deleted');
      })
    ));

    it('Should not call delete service on clear', () => {
      // GIVEN
      jest.spyOn(service, 'delete');

      // WHEN
      comp.cancel();

      // THEN
      expect(service.delete).not.toHaveBeenCalled();
      expect(mockActiveModal.close).not.toHaveBeenCalled();
      expect(mockActiveModal.dismiss).toHaveBeenCalled();
    });
  });
});
