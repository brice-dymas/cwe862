import { IClientBanque, NewClientBanque } from './client-banque.model';

export const sampleWithRequiredData: IClientBanque = {
  id: 15356,
  firstName: 'Osvaldo',
  lastName: 'Muller',
};

export const sampleWithPartialData: IClientBanque = {
  id: 8193,
  firstName: 'Curtis',
  lastName: 'Dicki',
};

export const sampleWithFullData: IClientBanque = {
  id: 15524,
  firstName: 'Vanessa',
  lastName: 'Aufderhar',
};

export const sampleWithNewData: NewClientBanque = {
  firstName: 'Heather',
  lastName: 'Ernser',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
