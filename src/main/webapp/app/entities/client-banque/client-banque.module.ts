import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ClientBanqueComponent } from './list/client-banque.component';
import { ClientBanqueDetailComponent } from './detail/client-banque-detail.component';
import { ClientBanqueUpdateComponent } from './update/client-banque-update.component';
import { ClientBanqueDeleteDialogComponent } from './delete/client-banque-delete-dialog.component';
import { ClientBanqueRoutingModule } from './route/client-banque-routing.module';

@NgModule({
  imports: [SharedModule, ClientBanqueRoutingModule],
  declarations: [ClientBanqueComponent, ClientBanqueDetailComponent, ClientBanqueUpdateComponent, ClientBanqueDeleteDialogComponent],
})
export class ClientBanqueModule {}
