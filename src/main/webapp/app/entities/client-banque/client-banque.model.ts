import { IUser } from 'app/entities/user/user.model';

export interface IClientBanque {
  id: number;
  firstName?: string | null;
  lastName?: string | null;
  user?: Pick<IUser, 'id'> | null;
}

export type NewClientBanque = Omit<IClientBanque, 'id'> & { id: null };
