import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ClientBanqueDetailComponent } from './client-banque-detail.component';

describe('ClientBanque Management Detail Component', () => {
  let comp: ClientBanqueDetailComponent;
  let fixture: ComponentFixture<ClientBanqueDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ClientBanqueDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ clientBanque: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ClientBanqueDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ClientBanqueDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load clientBanque on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.clientBanque).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
