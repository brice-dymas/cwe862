import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IClientBanque } from '../client-banque.model';

@Component({
  selector: 'jhi-client-banque-detail',
  templateUrl: './client-banque-detail.component.html',
})
export class ClientBanqueDetailComponent implements OnInit {
  clientBanque: IClientBanque | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ clientBanque }) => {
      this.clientBanque = clientBanque;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
