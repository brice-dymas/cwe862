import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../client-banque.test-samples';

import { ClientBanqueFormService } from './client-banque-form.service';

describe('ClientBanque Form Service', () => {
  let service: ClientBanqueFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClientBanqueFormService);
  });

  describe('Service methods', () => {
    describe('createClientBanqueFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createClientBanqueFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            firstName: expect.any(Object),
            lastName: expect.any(Object),
            user: expect.any(Object),
          })
        );
      });

      it('passing IClientBanque should create a new form with FormGroup', () => {
        const formGroup = service.createClientBanqueFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            firstName: expect.any(Object),
            lastName: expect.any(Object),
            user: expect.any(Object),
          })
        );
      });
    });

    describe('getClientBanque', () => {
      it('should return NewClientBanque for default ClientBanque initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createClientBanqueFormGroup(sampleWithNewData);

        const clientBanque = service.getClientBanque(formGroup) as any;

        expect(clientBanque).toMatchObject(sampleWithNewData);
      });

      it('should return NewClientBanque for empty ClientBanque initial value', () => {
        const formGroup = service.createClientBanqueFormGroup();

        const clientBanque = service.getClientBanque(formGroup) as any;

        expect(clientBanque).toMatchObject({});
      });

      it('should return IClientBanque', () => {
        const formGroup = service.createClientBanqueFormGroup(sampleWithRequiredData);

        const clientBanque = service.getClientBanque(formGroup) as any;

        expect(clientBanque).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IClientBanque should not enable id FormControl', () => {
        const formGroup = service.createClientBanqueFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewClientBanque should disable id FormControl', () => {
        const formGroup = service.createClientBanqueFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
