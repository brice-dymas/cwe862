import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ClientBanqueFormService, ClientBanqueFormGroup } from './client-banque-form.service';
import { IClientBanque } from '../client-banque.model';
import { ClientBanqueService } from '../service/client-banque.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';

@Component({
  selector: 'jhi-client-banque-update',
  templateUrl: './client-banque-update.component.html',
})
export class ClientBanqueUpdateComponent implements OnInit {
  isSaving = false;
  clientBanque: IClientBanque | null = null;

  usersSharedCollection: IUser[] = [];

  editForm: ClientBanqueFormGroup = this.clientBanqueFormService.createClientBanqueFormGroup();

  constructor(
    protected clientBanqueService: ClientBanqueService,
    protected clientBanqueFormService: ClientBanqueFormService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareUser = (o1: IUser | null, o2: IUser | null): boolean => this.userService.compareUser(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ clientBanque }) => {
      this.clientBanque = clientBanque;
      if (clientBanque) {
        this.updateForm(clientBanque);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const clientBanque = this.clientBanqueFormService.getClientBanque(this.editForm);
    if (clientBanque.id !== null) {
      this.subscribeToSaveResponse(this.clientBanqueService.update(clientBanque));
    } else {
      this.subscribeToSaveResponse(this.clientBanqueService.create(clientBanque));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IClientBanque>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(clientBanque: IClientBanque): void {
    this.clientBanque = clientBanque;
    this.clientBanqueFormService.resetForm(this.editForm, clientBanque);

    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing<IUser>(this.usersSharedCollection, clientBanque.user);
  }

  protected loadRelationshipsOptions(): void {
    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing<IUser>(users, this.clientBanque?.user)))
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));
  }
}
