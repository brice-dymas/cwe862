import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IClientBanque, NewClientBanque } from '../client-banque.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IClientBanque for edit and NewClientBanqueFormGroupInput for create.
 */
type ClientBanqueFormGroupInput = IClientBanque | PartialWithRequiredKeyOf<NewClientBanque>;

type ClientBanqueFormDefaults = Pick<NewClientBanque, 'id'>;

type ClientBanqueFormGroupContent = {
  id: FormControl<IClientBanque['id'] | NewClientBanque['id']>;
  firstName: FormControl<IClientBanque['firstName']>;
  lastName: FormControl<IClientBanque['lastName']>;
  user: FormControl<IClientBanque['user']>;
};

export type ClientBanqueFormGroup = FormGroup<ClientBanqueFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class ClientBanqueFormService {
  createClientBanqueFormGroup(clientBanque: ClientBanqueFormGroupInput = { id: null }): ClientBanqueFormGroup {
    const clientBanqueRawValue = {
      ...this.getFormDefaults(),
      ...clientBanque,
    };
    return new FormGroup<ClientBanqueFormGroupContent>({
      id: new FormControl(
        { value: clientBanqueRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      firstName: new FormControl(clientBanqueRawValue.firstName, {
        validators: [Validators.required],
      }),
      lastName: new FormControl(clientBanqueRawValue.lastName, {
        validators: [Validators.required],
      }),
      user: new FormControl(clientBanqueRawValue.user),
    });
  }

  getClientBanque(form: ClientBanqueFormGroup): IClientBanque | NewClientBanque {
    return form.getRawValue() as IClientBanque | NewClientBanque;
  }

  resetForm(form: ClientBanqueFormGroup, clientBanque: ClientBanqueFormGroupInput): void {
    const clientBanqueRawValue = { ...this.getFormDefaults(), ...clientBanque };
    form.reset(
      {
        ...clientBanqueRawValue,
        id: { value: clientBanqueRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): ClientBanqueFormDefaults {
    return {
      id: null,
    };
  }
}
