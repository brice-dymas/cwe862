import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IClientBanque, NewClientBanque } from '../client-banque.model';

export type PartialUpdateClientBanque = Partial<IClientBanque> & Pick<IClientBanque, 'id'>;

export type EntityResponseType = HttpResponse<IClientBanque>;
export type EntityArrayResponseType = HttpResponse<IClientBanque[]>;

@Injectable({ providedIn: 'root' })
export class ClientBanqueService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/client-banques');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(clientBanque: NewClientBanque): Observable<EntityResponseType> {
    return this.http.post<IClientBanque>(this.resourceUrl, clientBanque, { observe: 'response' });
  }

  update(clientBanque: IClientBanque): Observable<EntityResponseType> {
    return this.http.put<IClientBanque>(`${this.resourceUrl}/${this.getClientBanqueIdentifier(clientBanque)}`, clientBanque, {
      observe: 'response',
    });
  }

  partialUpdate(clientBanque: PartialUpdateClientBanque): Observable<EntityResponseType> {
    return this.http.patch<IClientBanque>(`${this.resourceUrl}/${this.getClientBanqueIdentifier(clientBanque)}`, clientBanque, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IClientBanque>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IClientBanque[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getClientBanqueIdentifier(clientBanque: Pick<IClientBanque, 'id'>): number {
    return clientBanque.id;
  }

  compareClientBanque(o1: Pick<IClientBanque, 'id'> | null, o2: Pick<IClientBanque, 'id'> | null): boolean {
    return o1 && o2 ? this.getClientBanqueIdentifier(o1) === this.getClientBanqueIdentifier(o2) : o1 === o2;
  }

  addClientBanqueToCollectionIfMissing<Type extends Pick<IClientBanque, 'id'>>(
    clientBanqueCollection: Type[],
    ...clientBanquesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const clientBanques: Type[] = clientBanquesToCheck.filter(isPresent);
    if (clientBanques.length > 0) {
      const clientBanqueCollectionIdentifiers = clientBanqueCollection.map(
        clientBanqueItem => this.getClientBanqueIdentifier(clientBanqueItem)!
      );
      const clientBanquesToAdd = clientBanques.filter(clientBanqueItem => {
        const clientBanqueIdentifier = this.getClientBanqueIdentifier(clientBanqueItem);
        if (clientBanqueCollectionIdentifiers.includes(clientBanqueIdentifier)) {
          return false;
        }
        clientBanqueCollectionIdentifiers.push(clientBanqueIdentifier);
        return true;
      });
      return [...clientBanquesToAdd, ...clientBanqueCollection];
    }
    return clientBanqueCollection;
  }
}
