import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ClientBanqueComponent } from '../list/client-banque.component';
import { ClientBanqueDetailComponent } from '../detail/client-banque-detail.component';
import { ClientBanqueUpdateComponent } from '../update/client-banque-update.component';
import { ClientBanqueRoutingResolveService } from './client-banque-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const clientBanqueRoute: Routes = [
  {
    path: '',
    component: ClientBanqueComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ClientBanqueDetailComponent,
    resolve: {
      clientBanque: ClientBanqueRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ClientBanqueUpdateComponent,
    resolve: {
      clientBanque: ClientBanqueRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ClientBanqueUpdateComponent,
    resolve: {
      clientBanque: ClientBanqueRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(clientBanqueRoute)],
  exports: [RouterModule],
})
export class ClientBanqueRoutingModule {}
