import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IClientBanque } from '../client-banque.model';
import { ClientBanqueService } from '../service/client-banque.service';

@Injectable({ providedIn: 'root' })
export class ClientBanqueRoutingResolveService implements Resolve<IClientBanque | null> {
  constructor(protected service: ClientBanqueService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IClientBanque | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((clientBanque: HttpResponse<IClientBanque>) => {
          if (clientBanque.body) {
            return of(clientBanque.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
