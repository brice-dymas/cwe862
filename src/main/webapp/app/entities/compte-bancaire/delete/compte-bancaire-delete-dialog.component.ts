import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICompteBancaire } from '../compte-bancaire.model';
import { CompteBancaireService } from '../service/compte-bancaire.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './compte-bancaire-delete-dialog.component.html',
})
export class CompteBancaireDeleteDialogComponent {
  compteBancaire?: ICompteBancaire;

  constructor(protected compteBancaireService: CompteBancaireService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.compteBancaireService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
