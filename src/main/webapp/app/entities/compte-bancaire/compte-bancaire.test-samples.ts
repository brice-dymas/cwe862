import { ICompteBancaire, NewCompteBancaire } from './compte-bancaire.model';

export const sampleWithRequiredData: ICompteBancaire = {
  id: 2066,
  numero: 'needs-based',
  solde: 'connect red',
};

export const sampleWithPartialData: ICompteBancaire = {
  id: 74050,
  numero: 'Senior Guinea blockchains',
  solde: 'Concrete enterprise',
};

export const sampleWithFullData: ICompteBancaire = {
  id: 61214,
  numero: 'stable strategize',
  solde: 'solution-oriented Account Solutions',
};

export const sampleWithNewData: NewCompteBancaire = {
  numero: 'Adaptive navigating array',
  solde: 'ADP',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
