import { IClientBanque } from 'app/entities/client-banque/client-banque.model';

export interface ICompteBancaire {
  id: number;
  numero?: string | null;
  solde?: string | null;
  titulaire?: Pick<IClientBanque, 'id'> | null;
}

export type NewCompteBancaire = Omit<ICompteBancaire, 'id'> & { id: null };
