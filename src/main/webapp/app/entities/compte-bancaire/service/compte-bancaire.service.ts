import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICompteBancaire, NewCompteBancaire } from '../compte-bancaire.model';

export type PartialUpdateCompteBancaire = Partial<ICompteBancaire> & Pick<ICompteBancaire, 'id'>;

export type EntityResponseType = HttpResponse<ICompteBancaire>;
export type EntityArrayResponseType = HttpResponse<ICompteBancaire[]>;

@Injectable({ providedIn: 'root' })
export class CompteBancaireService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/compte-bancaires');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(compteBancaire: NewCompteBancaire): Observable<EntityResponseType> {
    return this.http.post<ICompteBancaire>(this.resourceUrl, compteBancaire, { observe: 'response' });
  }

  update(compteBancaire: ICompteBancaire): Observable<EntityResponseType> {
    return this.http.put<ICompteBancaire>(`${this.resourceUrl}/${this.getCompteBancaireIdentifier(compteBancaire)}`, compteBancaire, {
      observe: 'response',
    });
  }

  partialUpdate(compteBancaire: PartialUpdateCompteBancaire): Observable<EntityResponseType> {
    return this.http.patch<ICompteBancaire>(`${this.resourceUrl}/${this.getCompteBancaireIdentifier(compteBancaire)}`, compteBancaire, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICompteBancaire>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICompteBancaire[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getCompteBancaireIdentifier(compteBancaire: Pick<ICompteBancaire, 'id'>): number {
    return compteBancaire.id;
  }

  compareCompteBancaire(o1: Pick<ICompteBancaire, 'id'> | null, o2: Pick<ICompteBancaire, 'id'> | null): boolean {
    return o1 && o2 ? this.getCompteBancaireIdentifier(o1) === this.getCompteBancaireIdentifier(o2) : o1 === o2;
  }

  addCompteBancaireToCollectionIfMissing<Type extends Pick<ICompteBancaire, 'id'>>(
    compteBancaireCollection: Type[],
    ...compteBancairesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const compteBancaires: Type[] = compteBancairesToCheck.filter(isPresent);
    if (compteBancaires.length > 0) {
      const compteBancaireCollectionIdentifiers = compteBancaireCollection.map(
        compteBancaireItem => this.getCompteBancaireIdentifier(compteBancaireItem)!
      );
      const compteBancairesToAdd = compteBancaires.filter(compteBancaireItem => {
        const compteBancaireIdentifier = this.getCompteBancaireIdentifier(compteBancaireItem);
        if (compteBancaireCollectionIdentifiers.includes(compteBancaireIdentifier)) {
          return false;
        }
        compteBancaireCollectionIdentifiers.push(compteBancaireIdentifier);
        return true;
      });
      return [...compteBancairesToAdd, ...compteBancaireCollection];
    }
    return compteBancaireCollection;
  }
}
