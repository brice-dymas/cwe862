import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ICompteBancaire, NewCompteBancaire } from '../compte-bancaire.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ICompteBancaire for edit and NewCompteBancaireFormGroupInput for create.
 */
type CompteBancaireFormGroupInput = ICompteBancaire | PartialWithRequiredKeyOf<NewCompteBancaire>;

type CompteBancaireFormDefaults = Pick<NewCompteBancaire, 'id'>;

type CompteBancaireFormGroupContent = {
  id: FormControl<ICompteBancaire['id'] | NewCompteBancaire['id']>;
  numero: FormControl<ICompteBancaire['numero']>;
  solde: FormControl<ICompteBancaire['solde']>;
  titulaire: FormControl<ICompteBancaire['titulaire']>;
};

export type CompteBancaireFormGroup = FormGroup<CompteBancaireFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class CompteBancaireFormService {
  createCompteBancaireFormGroup(compteBancaire: CompteBancaireFormGroupInput = { id: null }): CompteBancaireFormGroup {
    const compteBancaireRawValue = {
      ...this.getFormDefaults(),
      ...compteBancaire,
    };
    return new FormGroup<CompteBancaireFormGroupContent>({
      id: new FormControl(
        { value: compteBancaireRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      numero: new FormControl(compteBancaireRawValue.numero, {
        validators: [Validators.required],
      }),
      solde: new FormControl(compteBancaireRawValue.solde, {
        validators: [Validators.required],
      }),
      titulaire: new FormControl(compteBancaireRawValue.titulaire, {
        validators: [Validators.required],
      }),
    });
  }

  getCompteBancaire(form: CompteBancaireFormGroup): ICompteBancaire | NewCompteBancaire {
    return form.getRawValue() as ICompteBancaire | NewCompteBancaire;
  }

  resetForm(form: CompteBancaireFormGroup, compteBancaire: CompteBancaireFormGroupInput): void {
    const compteBancaireRawValue = { ...this.getFormDefaults(), ...compteBancaire };
    form.reset(
      {
        ...compteBancaireRawValue,
        id: { value: compteBancaireRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): CompteBancaireFormDefaults {
    return {
      id: null,
    };
  }
}
