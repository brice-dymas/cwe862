import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { CompteBancaireFormService, CompteBancaireFormGroup } from './compte-bancaire-form.service';
import { ICompteBancaire } from '../compte-bancaire.model';
import { CompteBancaireService } from '../service/compte-bancaire.service';
import { IClientBanque } from 'app/entities/client-banque/client-banque.model';
import { ClientBanqueService } from 'app/entities/client-banque/service/client-banque.service';

@Component({
  selector: 'jhi-compte-bancaire-update',
  templateUrl: './compte-bancaire-update.component.html',
})
export class CompteBancaireUpdateComponent implements OnInit {
  isSaving = false;
  compteBancaire: ICompteBancaire | null = null;

  clientBanquesSharedCollection: IClientBanque[] = [];

  editForm: CompteBancaireFormGroup = this.compteBancaireFormService.createCompteBancaireFormGroup();

  constructor(
    protected compteBancaireService: CompteBancaireService,
    protected compteBancaireFormService: CompteBancaireFormService,
    protected clientBanqueService: ClientBanqueService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareClientBanque = (o1: IClientBanque | null, o2: IClientBanque | null): boolean =>
    this.clientBanqueService.compareClientBanque(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ compteBancaire }) => {
      this.compteBancaire = compteBancaire;
      if (compteBancaire) {
        this.updateForm(compteBancaire);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const compteBancaire = this.compteBancaireFormService.getCompteBancaire(this.editForm);
    if (compteBancaire.id !== null) {
      this.subscribeToSaveResponse(this.compteBancaireService.update(compteBancaire));
    } else {
      this.subscribeToSaveResponse(this.compteBancaireService.create(compteBancaire));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICompteBancaire>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(compteBancaire: ICompteBancaire): void {
    this.compteBancaire = compteBancaire;
    this.compteBancaireFormService.resetForm(this.editForm, compteBancaire);

    this.clientBanquesSharedCollection = this.clientBanqueService.addClientBanqueToCollectionIfMissing<IClientBanque>(
      this.clientBanquesSharedCollection,
      compteBancaire.titulaire
    );
  }

  protected loadRelationshipsOptions(): void {
    this.clientBanqueService
      .query()
      .pipe(map((res: HttpResponse<IClientBanque[]>) => res.body ?? []))
      .pipe(
        map((clientBanques: IClientBanque[]) =>
          this.clientBanqueService.addClientBanqueToCollectionIfMissing<IClientBanque>(clientBanques, this.compteBancaire?.titulaire)
        )
      )
      .subscribe((clientBanques: IClientBanque[]) => (this.clientBanquesSharedCollection = clientBanques));
  }
}
