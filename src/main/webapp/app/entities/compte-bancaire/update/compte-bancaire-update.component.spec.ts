import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CompteBancaireFormService } from './compte-bancaire-form.service';
import { CompteBancaireService } from '../service/compte-bancaire.service';
import { ICompteBancaire } from '../compte-bancaire.model';
import { IClientBanque } from 'app/entities/client-banque/client-banque.model';
import { ClientBanqueService } from 'app/entities/client-banque/service/client-banque.service';

import { CompteBancaireUpdateComponent } from './compte-bancaire-update.component';

describe('CompteBancaire Management Update Component', () => {
  let comp: CompteBancaireUpdateComponent;
  let fixture: ComponentFixture<CompteBancaireUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let compteBancaireFormService: CompteBancaireFormService;
  let compteBancaireService: CompteBancaireService;
  let clientBanqueService: ClientBanqueService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CompteBancaireUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CompteBancaireUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CompteBancaireUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    compteBancaireFormService = TestBed.inject(CompteBancaireFormService);
    compteBancaireService = TestBed.inject(CompteBancaireService);
    clientBanqueService = TestBed.inject(ClientBanqueService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call ClientBanque query and add missing value', () => {
      const compteBancaire: ICompteBancaire = { id: 456 };
      const titulaire: IClientBanque = { id: 30022 };
      compteBancaire.titulaire = titulaire;

      const clientBanqueCollection: IClientBanque[] = [{ id: 49531 }];
      jest.spyOn(clientBanqueService, 'query').mockReturnValue(of(new HttpResponse({ body: clientBanqueCollection })));
      const additionalClientBanques = [titulaire];
      const expectedCollection: IClientBanque[] = [...additionalClientBanques, ...clientBanqueCollection];
      jest.spyOn(clientBanqueService, 'addClientBanqueToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ compteBancaire });
      comp.ngOnInit();

      expect(clientBanqueService.query).toHaveBeenCalled();
      expect(clientBanqueService.addClientBanqueToCollectionIfMissing).toHaveBeenCalledWith(
        clientBanqueCollection,
        ...additionalClientBanques.map(expect.objectContaining)
      );
      expect(comp.clientBanquesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const compteBancaire: ICompteBancaire = { id: 456 };
      const titulaire: IClientBanque = { id: 47058 };
      compteBancaire.titulaire = titulaire;

      activatedRoute.data = of({ compteBancaire });
      comp.ngOnInit();

      expect(comp.clientBanquesSharedCollection).toContain(titulaire);
      expect(comp.compteBancaire).toEqual(compteBancaire);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICompteBancaire>>();
      const compteBancaire = { id: 123 };
      jest.spyOn(compteBancaireFormService, 'getCompteBancaire').mockReturnValue(compteBancaire);
      jest.spyOn(compteBancaireService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ compteBancaire });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: compteBancaire }));
      saveSubject.complete();

      // THEN
      expect(compteBancaireFormService.getCompteBancaire).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(compteBancaireService.update).toHaveBeenCalledWith(expect.objectContaining(compteBancaire));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICompteBancaire>>();
      const compteBancaire = { id: 123 };
      jest.spyOn(compteBancaireFormService, 'getCompteBancaire').mockReturnValue({ id: null });
      jest.spyOn(compteBancaireService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ compteBancaire: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: compteBancaire }));
      saveSubject.complete();

      // THEN
      expect(compteBancaireFormService.getCompteBancaire).toHaveBeenCalled();
      expect(compteBancaireService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICompteBancaire>>();
      const compteBancaire = { id: 123 };
      jest.spyOn(compteBancaireService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ compteBancaire });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(compteBancaireService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareClientBanque', () => {
      it('Should forward to clientBanqueService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(clientBanqueService, 'compareClientBanque');
        comp.compareClientBanque(entity, entity2);
        expect(clientBanqueService.compareClientBanque).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
