import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../compte-bancaire.test-samples';

import { CompteBancaireFormService } from './compte-bancaire-form.service';

describe('CompteBancaire Form Service', () => {
  let service: CompteBancaireFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CompteBancaireFormService);
  });

  describe('Service methods', () => {
    describe('createCompteBancaireFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createCompteBancaireFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            numero: expect.any(Object),
            solde: expect.any(Object),
            titulaire: expect.any(Object),
          })
        );
      });

      it('passing ICompteBancaire should create a new form with FormGroup', () => {
        const formGroup = service.createCompteBancaireFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            numero: expect.any(Object),
            solde: expect.any(Object),
            titulaire: expect.any(Object),
          })
        );
      });
    });

    describe('getCompteBancaire', () => {
      it('should return NewCompteBancaire for default CompteBancaire initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createCompteBancaireFormGroup(sampleWithNewData);

        const compteBancaire = service.getCompteBancaire(formGroup) as any;

        expect(compteBancaire).toMatchObject(sampleWithNewData);
      });

      it('should return NewCompteBancaire for empty CompteBancaire initial value', () => {
        const formGroup = service.createCompteBancaireFormGroup();

        const compteBancaire = service.getCompteBancaire(formGroup) as any;

        expect(compteBancaire).toMatchObject({});
      });

      it('should return ICompteBancaire', () => {
        const formGroup = service.createCompteBancaireFormGroup(sampleWithRequiredData);

        const compteBancaire = service.getCompteBancaire(formGroup) as any;

        expect(compteBancaire).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ICompteBancaire should not enable id FormControl', () => {
        const formGroup = service.createCompteBancaireFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewCompteBancaire should disable id FormControl', () => {
        const formGroup = service.createCompteBancaireFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
