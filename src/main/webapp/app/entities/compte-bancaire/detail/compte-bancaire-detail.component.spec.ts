import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CompteBancaireDetailComponent } from './compte-bancaire-detail.component';

describe('CompteBancaire Management Detail Component', () => {
  let comp: CompteBancaireDetailComponent;
  let fixture: ComponentFixture<CompteBancaireDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CompteBancaireDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ compteBancaire: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CompteBancaireDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CompteBancaireDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load compteBancaire on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.compteBancaire).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
