import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICompteBancaire } from '../compte-bancaire.model';

@Component({
  selector: 'jhi-compte-bancaire-detail',
  templateUrl: './compte-bancaire-detail.component.html',
})
export class CompteBancaireDetailComponent implements OnInit {
  compteBancaire: ICompteBancaire | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ compteBancaire }) => {
      this.compteBancaire = compteBancaire;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
