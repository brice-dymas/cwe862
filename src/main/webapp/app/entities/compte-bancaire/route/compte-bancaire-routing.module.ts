import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CompteBancaireComponent } from '../list/compte-bancaire.component';
import { CompteBancaireDetailComponent } from '../detail/compte-bancaire-detail.component';
import { CompteBancaireUpdateComponent } from '../update/compte-bancaire-update.component';
import { CompteBancaireRoutingResolveService } from './compte-bancaire-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const compteBancaireRoute: Routes = [
  {
    path: '',
    component: CompteBancaireComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CompteBancaireDetailComponent,
    resolve: {
      compteBancaire: CompteBancaireRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CompteBancaireUpdateComponent,
    resolve: {
      compteBancaire: CompteBancaireRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CompteBancaireUpdateComponent,
    resolve: {
      compteBancaire: CompteBancaireRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(compteBancaireRoute)],
  exports: [RouterModule],
})
export class CompteBancaireRoutingModule {}
