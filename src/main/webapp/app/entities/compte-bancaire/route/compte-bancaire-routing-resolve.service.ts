import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICompteBancaire } from '../compte-bancaire.model';
import { CompteBancaireService } from '../service/compte-bancaire.service';

@Injectable({ providedIn: 'root' })
export class CompteBancaireRoutingResolveService implements Resolve<ICompteBancaire | null> {
  constructor(protected service: CompteBancaireService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICompteBancaire | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((compteBancaire: HttpResponse<ICompteBancaire>) => {
          if (compteBancaire.body) {
            return of(compteBancaire.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
