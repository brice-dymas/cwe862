import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CompteBancaireComponent } from './list/compte-bancaire.component';
import { CompteBancaireDetailComponent } from './detail/compte-bancaire-detail.component';
import { CompteBancaireUpdateComponent } from './update/compte-bancaire-update.component';
import { CompteBancaireDeleteDialogComponent } from './delete/compte-bancaire-delete-dialog.component';
import { CompteBancaireRoutingModule } from './route/compte-bancaire-routing.module';

@NgModule({
  imports: [SharedModule, CompteBancaireRoutingModule],
  declarations: [
    CompteBancaireComponent,
    CompteBancaireDetailComponent,
    CompteBancaireUpdateComponent,
    CompteBancaireDeleteDialogComponent,
  ],
})
export class CompteBancaireModule {}
