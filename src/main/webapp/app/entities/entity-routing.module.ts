import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'compte-bancaire',
        data: { pageTitle: 'CompteBancaires' },
        loadChildren: () => import('./compte-bancaire/compte-bancaire.module').then(m => m.CompteBancaireModule),
      },
      {
        path: 'client-banque',
        data: { pageTitle: 'ClientBanques' },
        loadChildren: () => import('./client-banque/client-banque.module').then(m => m.ClientBanqueModule),
      },
      {
        path: 'virement-bancaire',
        data: { pageTitle: 'VirementBancaires' },
        loadChildren: () => import('./virement-bancaire/virement-bancaire.module').then(m => m.VirementBancaireModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
