import dayjs from 'dayjs/esm';
import { ICompteBancaire } from 'app/entities/compte-bancaire/compte-bancaire.model';

export interface IVirementBancaire {
  id: number;
  dateVirement?: dayjs.Dayjs | null;
  montant?: number | null;
  compteSource?: Pick<ICompteBancaire, 'id'> | null;
  compteDestination?: Pick<ICompteBancaire, 'id'> | null;
}

export type NewVirementBancaire = Omit<IVirementBancaire, 'id'> & { id: null };
