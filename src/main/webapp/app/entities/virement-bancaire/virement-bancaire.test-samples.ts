import dayjs from 'dayjs/esm';

import { IVirementBancaire, NewVirementBancaire } from './virement-bancaire.model';

export const sampleWithRequiredData: IVirementBancaire = {
  id: 77499,
  montant: 87734,
};

export const sampleWithPartialData: IVirementBancaire = {
  id: 26591,
  dateVirement: dayjs('2023-05-09T02:17'),
  montant: 55410,
};

export const sampleWithFullData: IVirementBancaire = {
  id: 79501,
  dateVirement: dayjs('2023-05-09T03:38'),
  montant: 61965,
};

export const sampleWithNewData: NewVirementBancaire = {
  montant: 30338,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
