import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IVirementBancaire, NewVirementBancaire } from '../virement-bancaire.model';

export type PartialUpdateVirementBancaire = Partial<IVirementBancaire> & Pick<IVirementBancaire, 'id'>;

type RestOf<T extends IVirementBancaire | NewVirementBancaire> = Omit<T, 'dateVirement'> & {
  dateVirement?: string | null;
};

export type RestVirementBancaire = RestOf<IVirementBancaire>;

export type NewRestVirementBancaire = RestOf<NewVirementBancaire>;

export type PartialUpdateRestVirementBancaire = RestOf<PartialUpdateVirementBancaire>;

export type EntityResponseType = HttpResponse<IVirementBancaire>;
export type EntityArrayResponseType = HttpResponse<IVirementBancaire[]>;

@Injectable({ providedIn: 'root' })
export class VirementBancaireService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/virement-bancaires');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(virementBancaire: NewVirementBancaire): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(virementBancaire);
    return this.http
      .post<RestVirementBancaire>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(virementBancaire: IVirementBancaire): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(virementBancaire);
    return this.http
      .put<RestVirementBancaire>(`${this.resourceUrl}/${this.getVirementBancaireIdentifier(virementBancaire)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(virementBancaire: PartialUpdateVirementBancaire): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(virementBancaire);
    return this.http
      .patch<RestVirementBancaire>(`${this.resourceUrl}/${this.getVirementBancaireIdentifier(virementBancaire)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestVirementBancaire>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestVirementBancaire[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getVirementBancaireIdentifier(virementBancaire: Pick<IVirementBancaire, 'id'>): number {
    return virementBancaire.id;
  }

  compareVirementBancaire(o1: Pick<IVirementBancaire, 'id'> | null, o2: Pick<IVirementBancaire, 'id'> | null): boolean {
    return o1 && o2 ? this.getVirementBancaireIdentifier(o1) === this.getVirementBancaireIdentifier(o2) : o1 === o2;
  }

  addVirementBancaireToCollectionIfMissing<Type extends Pick<IVirementBancaire, 'id'>>(
    virementBancaireCollection: Type[],
    ...virementBancairesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const virementBancaires: Type[] = virementBancairesToCheck.filter(isPresent);
    if (virementBancaires.length > 0) {
      const virementBancaireCollectionIdentifiers = virementBancaireCollection.map(
        virementBancaireItem => this.getVirementBancaireIdentifier(virementBancaireItem)!
      );
      const virementBancairesToAdd = virementBancaires.filter(virementBancaireItem => {
        const virementBancaireIdentifier = this.getVirementBancaireIdentifier(virementBancaireItem);
        if (virementBancaireCollectionIdentifiers.includes(virementBancaireIdentifier)) {
          return false;
        }
        virementBancaireCollectionIdentifiers.push(virementBancaireIdentifier);
        return true;
      });
      return [...virementBancairesToAdd, ...virementBancaireCollection];
    }
    return virementBancaireCollection;
  }

  protected convertDateFromClient<T extends IVirementBancaire | NewVirementBancaire | PartialUpdateVirementBancaire>(
    virementBancaire: T
  ): RestOf<T> {
    return {
      ...virementBancaire,
      dateVirement: virementBancaire.dateVirement?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restVirementBancaire: RestVirementBancaire): IVirementBancaire {
    return {
      ...restVirementBancaire,
      dateVirement: restVirementBancaire.dateVirement ? dayjs(restVirementBancaire.dateVirement) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestVirementBancaire>): HttpResponse<IVirementBancaire> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestVirementBancaire[]>): HttpResponse<IVirementBancaire[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
