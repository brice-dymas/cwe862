import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IVirementBancaire } from '../virement-bancaire.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../virement-bancaire.test-samples';

import { VirementBancaireService, RestVirementBancaire } from './virement-bancaire.service';

const requireRestSample: RestVirementBancaire = {
  ...sampleWithRequiredData,
  dateVirement: sampleWithRequiredData.dateVirement?.toJSON(),
};

describe('VirementBancaire Service', () => {
  let service: VirementBancaireService;
  let httpMock: HttpTestingController;
  let expectedResult: IVirementBancaire | IVirementBancaire[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(VirementBancaireService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a VirementBancaire', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const virementBancaire = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(virementBancaire).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a VirementBancaire', () => {
      const virementBancaire = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(virementBancaire).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a VirementBancaire', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of VirementBancaire', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a VirementBancaire', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addVirementBancaireToCollectionIfMissing', () => {
      it('should add a VirementBancaire to an empty array', () => {
        const virementBancaire: IVirementBancaire = sampleWithRequiredData;
        expectedResult = service.addVirementBancaireToCollectionIfMissing([], virementBancaire);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(virementBancaire);
      });

      it('should not add a VirementBancaire to an array that contains it', () => {
        const virementBancaire: IVirementBancaire = sampleWithRequiredData;
        const virementBancaireCollection: IVirementBancaire[] = [
          {
            ...virementBancaire,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addVirementBancaireToCollectionIfMissing(virementBancaireCollection, virementBancaire);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a VirementBancaire to an array that doesn't contain it", () => {
        const virementBancaire: IVirementBancaire = sampleWithRequiredData;
        const virementBancaireCollection: IVirementBancaire[] = [sampleWithPartialData];
        expectedResult = service.addVirementBancaireToCollectionIfMissing(virementBancaireCollection, virementBancaire);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(virementBancaire);
      });

      it('should add only unique VirementBancaire to an array', () => {
        const virementBancaireArray: IVirementBancaire[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const virementBancaireCollection: IVirementBancaire[] = [sampleWithRequiredData];
        expectedResult = service.addVirementBancaireToCollectionIfMissing(virementBancaireCollection, ...virementBancaireArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const virementBancaire: IVirementBancaire = sampleWithRequiredData;
        const virementBancaire2: IVirementBancaire = sampleWithPartialData;
        expectedResult = service.addVirementBancaireToCollectionIfMissing([], virementBancaire, virementBancaire2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(virementBancaire);
        expect(expectedResult).toContain(virementBancaire2);
      });

      it('should accept null and undefined values', () => {
        const virementBancaire: IVirementBancaire = sampleWithRequiredData;
        expectedResult = service.addVirementBancaireToCollectionIfMissing([], null, virementBancaire, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(virementBancaire);
      });

      it('should return initial array if no VirementBancaire is added', () => {
        const virementBancaireCollection: IVirementBancaire[] = [sampleWithRequiredData];
        expectedResult = service.addVirementBancaireToCollectionIfMissing(virementBancaireCollection, undefined, null);
        expect(expectedResult).toEqual(virementBancaireCollection);
      });
    });

    describe('compareVirementBancaire', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareVirementBancaire(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareVirementBancaire(entity1, entity2);
        const compareResult2 = service.compareVirementBancaire(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareVirementBancaire(entity1, entity2);
        const compareResult2 = service.compareVirementBancaire(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareVirementBancaire(entity1, entity2);
        const compareResult2 = service.compareVirementBancaire(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
