import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IVirementBancaire } from '../virement-bancaire.model';
import { VirementBancaireService } from '../service/virement-bancaire.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './virement-bancaire-delete-dialog.component.html',
})
export class VirementBancaireDeleteDialogComponent {
  virementBancaire?: IVirementBancaire;

  constructor(protected virementBancaireService: VirementBancaireService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.virementBancaireService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
