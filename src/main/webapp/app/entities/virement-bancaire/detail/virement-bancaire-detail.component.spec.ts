import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { VirementBancaireDetailComponent } from './virement-bancaire-detail.component';

describe('VirementBancaire Management Detail Component', () => {
  let comp: VirementBancaireDetailComponent;
  let fixture: ComponentFixture<VirementBancaireDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VirementBancaireDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ virementBancaire: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(VirementBancaireDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(VirementBancaireDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load virementBancaire on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.virementBancaire).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
