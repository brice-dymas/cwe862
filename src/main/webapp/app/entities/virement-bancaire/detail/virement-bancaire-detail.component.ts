import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IVirementBancaire } from '../virement-bancaire.model';

@Component({
  selector: 'jhi-virement-bancaire-detail',
  templateUrl: './virement-bancaire-detail.component.html',
})
export class VirementBancaireDetailComponent implements OnInit {
  virementBancaire: IVirementBancaire | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ virementBancaire }) => {
      this.virementBancaire = virementBancaire;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
