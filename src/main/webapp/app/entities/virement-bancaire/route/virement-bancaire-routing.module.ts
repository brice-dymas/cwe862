import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { VirementBancaireComponent } from '../list/virement-bancaire.component';
import { VirementBancaireDetailComponent } from '../detail/virement-bancaire-detail.component';
import { VirementBancaireUpdateComponent } from '../update/virement-bancaire-update.component';
import { VirementBancaireRoutingResolveService } from './virement-bancaire-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const virementBancaireRoute: Routes = [
  {
    path: '',
    component: VirementBancaireComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: VirementBancaireDetailComponent,
    resolve: {
      virementBancaire: VirementBancaireRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: VirementBancaireUpdateComponent,
    resolve: {
      virementBancaire: VirementBancaireRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: VirementBancaireUpdateComponent,
    resolve: {
      virementBancaire: VirementBancaireRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(virementBancaireRoute)],
  exports: [RouterModule],
})
export class VirementBancaireRoutingModule {}
