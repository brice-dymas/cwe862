import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IVirementBancaire } from '../virement-bancaire.model';
import { VirementBancaireService } from '../service/virement-bancaire.service';

@Injectable({ providedIn: 'root' })
export class VirementBancaireRoutingResolveService implements Resolve<IVirementBancaire | null> {
  constructor(protected service: VirementBancaireService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IVirementBancaire | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((virementBancaire: HttpResponse<IVirementBancaire>) => {
          if (virementBancaire.body) {
            return of(virementBancaire.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
