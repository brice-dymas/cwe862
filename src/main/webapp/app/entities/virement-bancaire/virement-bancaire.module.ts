import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { VirementBancaireComponent } from './list/virement-bancaire.component';
import { VirementBancaireDetailComponent } from './detail/virement-bancaire-detail.component';
import { VirementBancaireUpdateComponent } from './update/virement-bancaire-update.component';
import { VirementBancaireDeleteDialogComponent } from './delete/virement-bancaire-delete-dialog.component';
import { VirementBancaireRoutingModule } from './route/virement-bancaire-routing.module';

@NgModule({
  imports: [SharedModule, VirementBancaireRoutingModule],
  declarations: [
    VirementBancaireComponent,
    VirementBancaireDetailComponent,
    VirementBancaireUpdateComponent,
    VirementBancaireDeleteDialogComponent,
  ],
})
export class VirementBancaireModule {}
