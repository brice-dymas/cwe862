import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { VirementBancaireFormService, VirementBancaireFormGroup } from './virement-bancaire-form.service';
import { IVirementBancaire } from '../virement-bancaire.model';
import { VirementBancaireService } from '../service/virement-bancaire.service';
import { ICompteBancaire } from 'app/entities/compte-bancaire/compte-bancaire.model';
import { CompteBancaireService } from 'app/entities/compte-bancaire/service/compte-bancaire.service';

@Component({
  selector: 'jhi-virement-bancaire-update',
  templateUrl: './virement-bancaire-update.component.html',
})
export class VirementBancaireUpdateComponent implements OnInit {
  isSaving = false;
  virementBancaire: IVirementBancaire | null = null;

  compteBancairesSharedCollection: ICompteBancaire[] = [];

  editForm: VirementBancaireFormGroup = this.virementBancaireFormService.createVirementBancaireFormGroup();

  constructor(
    protected virementBancaireService: VirementBancaireService,
    protected virementBancaireFormService: VirementBancaireFormService,
    protected compteBancaireService: CompteBancaireService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareCompteBancaire = (o1: ICompteBancaire | null, o2: ICompteBancaire | null): boolean =>
    this.compteBancaireService.compareCompteBancaire(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ virementBancaire }) => {
      this.virementBancaire = virementBancaire;
      if (virementBancaire) {
        this.updateForm(virementBancaire);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const virementBancaire = this.virementBancaireFormService.getVirementBancaire(this.editForm);
    if (virementBancaire.id !== null) {
      this.subscribeToSaveResponse(this.virementBancaireService.update(virementBancaire));
    } else {
      this.subscribeToSaveResponse(this.virementBancaireService.create(virementBancaire));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVirementBancaire>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(virementBancaire: IVirementBancaire): void {
    this.virementBancaire = virementBancaire;
    this.virementBancaireFormService.resetForm(this.editForm, virementBancaire);

    this.compteBancairesSharedCollection = this.compteBancaireService.addCompteBancaireToCollectionIfMissing<ICompteBancaire>(
      this.compteBancairesSharedCollection,
      virementBancaire.compteSource,
      virementBancaire.compteDestination
    );
  }

  protected loadRelationshipsOptions(): void {
    this.compteBancaireService
      .query()
      .pipe(map((res: HttpResponse<ICompteBancaire[]>) => res.body ?? []))
      .pipe(
        map((compteBancaires: ICompteBancaire[]) =>
          this.compteBancaireService.addCompteBancaireToCollectionIfMissing<ICompteBancaire>(
            compteBancaires,
            this.virementBancaire?.compteSource,
            this.virementBancaire?.compteDestination
          )
        )
      )
      .subscribe((compteBancaires: ICompteBancaire[]) => (this.compteBancairesSharedCollection = compteBancaires));
  }
}
