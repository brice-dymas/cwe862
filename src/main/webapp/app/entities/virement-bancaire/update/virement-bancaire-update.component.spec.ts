import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { VirementBancaireFormService } from './virement-bancaire-form.service';
import { VirementBancaireService } from '../service/virement-bancaire.service';
import { IVirementBancaire } from '../virement-bancaire.model';
import { ICompteBancaire } from 'app/entities/compte-bancaire/compte-bancaire.model';
import { CompteBancaireService } from 'app/entities/compte-bancaire/service/compte-bancaire.service';

import { VirementBancaireUpdateComponent } from './virement-bancaire-update.component';

describe('VirementBancaire Management Update Component', () => {
  let comp: VirementBancaireUpdateComponent;
  let fixture: ComponentFixture<VirementBancaireUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let virementBancaireFormService: VirementBancaireFormService;
  let virementBancaireService: VirementBancaireService;
  let compteBancaireService: CompteBancaireService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [VirementBancaireUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(VirementBancaireUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(VirementBancaireUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    virementBancaireFormService = TestBed.inject(VirementBancaireFormService);
    virementBancaireService = TestBed.inject(VirementBancaireService);
    compteBancaireService = TestBed.inject(CompteBancaireService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call CompteBancaire query and add missing value', () => {
      const virementBancaire: IVirementBancaire = { id: 456 };
      const compteSource: ICompteBancaire = { id: 42124 };
      virementBancaire.compteSource = compteSource;
      const compteDestination: ICompteBancaire = { id: 83884 };
      virementBancaire.compteDestination = compteDestination;

      const compteBancaireCollection: ICompteBancaire[] = [{ id: 14034 }];
      jest.spyOn(compteBancaireService, 'query').mockReturnValue(of(new HttpResponse({ body: compteBancaireCollection })));
      const additionalCompteBancaires = [compteSource, compteDestination];
      const expectedCollection: ICompteBancaire[] = [...additionalCompteBancaires, ...compteBancaireCollection];
      jest.spyOn(compteBancaireService, 'addCompteBancaireToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ virementBancaire });
      comp.ngOnInit();

      expect(compteBancaireService.query).toHaveBeenCalled();
      expect(compteBancaireService.addCompteBancaireToCollectionIfMissing).toHaveBeenCalledWith(
        compteBancaireCollection,
        ...additionalCompteBancaires.map(expect.objectContaining)
      );
      expect(comp.compteBancairesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const virementBancaire: IVirementBancaire = { id: 456 };
      const compteSource: ICompteBancaire = { id: 44030 };
      virementBancaire.compteSource = compteSource;
      const compteDestination: ICompteBancaire = { id: 49378 };
      virementBancaire.compteDestination = compteDestination;

      activatedRoute.data = of({ virementBancaire });
      comp.ngOnInit();

      expect(comp.compteBancairesSharedCollection).toContain(compteSource);
      expect(comp.compteBancairesSharedCollection).toContain(compteDestination);
      expect(comp.virementBancaire).toEqual(virementBancaire);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IVirementBancaire>>();
      const virementBancaire = { id: 123 };
      jest.spyOn(virementBancaireFormService, 'getVirementBancaire').mockReturnValue(virementBancaire);
      jest.spyOn(virementBancaireService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ virementBancaire });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: virementBancaire }));
      saveSubject.complete();

      // THEN
      expect(virementBancaireFormService.getVirementBancaire).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(virementBancaireService.update).toHaveBeenCalledWith(expect.objectContaining(virementBancaire));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IVirementBancaire>>();
      const virementBancaire = { id: 123 };
      jest.spyOn(virementBancaireFormService, 'getVirementBancaire').mockReturnValue({ id: null });
      jest.spyOn(virementBancaireService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ virementBancaire: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: virementBancaire }));
      saveSubject.complete();

      // THEN
      expect(virementBancaireFormService.getVirementBancaire).toHaveBeenCalled();
      expect(virementBancaireService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IVirementBancaire>>();
      const virementBancaire = { id: 123 };
      jest.spyOn(virementBancaireService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ virementBancaire });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(virementBancaireService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareCompteBancaire', () => {
      it('Should forward to compteBancaireService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(compteBancaireService, 'compareCompteBancaire');
        comp.compareCompteBancaire(entity, entity2);
        expect(compteBancaireService.compareCompteBancaire).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
