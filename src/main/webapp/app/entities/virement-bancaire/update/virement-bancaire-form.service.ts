import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IVirementBancaire, NewVirementBancaire } from '../virement-bancaire.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IVirementBancaire for edit and NewVirementBancaireFormGroupInput for create.
 */
type VirementBancaireFormGroupInput = IVirementBancaire | PartialWithRequiredKeyOf<NewVirementBancaire>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IVirementBancaire | NewVirementBancaire> = Omit<T, 'dateVirement'> & {
  dateVirement?: string | null;
};

type VirementBancaireFormRawValue = FormValueOf<IVirementBancaire>;

type NewVirementBancaireFormRawValue = FormValueOf<NewVirementBancaire>;

type VirementBancaireFormDefaults = Pick<NewVirementBancaire, 'id' | 'dateVirement'>;

type VirementBancaireFormGroupContent = {
  id: FormControl<VirementBancaireFormRawValue['id'] | NewVirementBancaire['id']>;
  dateVirement: FormControl<VirementBancaireFormRawValue['dateVirement']>;
  montant: FormControl<VirementBancaireFormRawValue['montant']>;
  compteSource: FormControl<VirementBancaireFormRawValue['compteSource']>;
  compteDestination: FormControl<VirementBancaireFormRawValue['compteDestination']>;
};

export type VirementBancaireFormGroup = FormGroup<VirementBancaireFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class VirementBancaireFormService {
  createVirementBancaireFormGroup(virementBancaire: VirementBancaireFormGroupInput = { id: null }): VirementBancaireFormGroup {
    const virementBancaireRawValue = this.convertVirementBancaireToVirementBancaireRawValue({
      ...this.getFormDefaults(),
      ...virementBancaire,
    });
    return new FormGroup<VirementBancaireFormGroupContent>({
      id: new FormControl(
        { value: virementBancaireRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      dateVirement: new FormControl(virementBancaireRawValue.dateVirement),
      montant: new FormControl(virementBancaireRawValue.montant, {
        validators: [Validators.required],
      }),
      compteSource: new FormControl(virementBancaireRawValue.compteSource, {
        validators: [Validators.required],
      }),
      compteDestination: new FormControl(virementBancaireRawValue.compteDestination, {
        validators: [Validators.required],
      }),
    });
  }

  getVirementBancaire(form: VirementBancaireFormGroup): IVirementBancaire | NewVirementBancaire {
    return this.convertVirementBancaireRawValueToVirementBancaire(
      form.getRawValue() as VirementBancaireFormRawValue | NewVirementBancaireFormRawValue
    );
  }

  resetForm(form: VirementBancaireFormGroup, virementBancaire: VirementBancaireFormGroupInput): void {
    const virementBancaireRawValue = this.convertVirementBancaireToVirementBancaireRawValue({
      ...this.getFormDefaults(),
      ...virementBancaire,
    });
    form.reset(
      {
        ...virementBancaireRawValue,
        id: { value: virementBancaireRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): VirementBancaireFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      dateVirement: currentTime,
    };
  }

  private convertVirementBancaireRawValueToVirementBancaire(
    rawVirementBancaire: VirementBancaireFormRawValue | NewVirementBancaireFormRawValue
  ): IVirementBancaire | NewVirementBancaire {
    return {
      ...rawVirementBancaire,
      dateVirement: dayjs(rawVirementBancaire.dateVirement, DATE_TIME_FORMAT),
    };
  }

  private convertVirementBancaireToVirementBancaireRawValue(
    virementBancaire: IVirementBancaire | (Partial<NewVirementBancaire> & VirementBancaireFormDefaults)
  ): VirementBancaireFormRawValue | PartialWithRequiredKeyOf<NewVirementBancaireFormRawValue> {
    return {
      ...virementBancaire,
      dateVirement: virementBancaire.dateVirement ? virementBancaire.dateVirement.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
