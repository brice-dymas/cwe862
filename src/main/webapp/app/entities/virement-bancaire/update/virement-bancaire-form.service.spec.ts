import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../virement-bancaire.test-samples';

import { VirementBancaireFormService } from './virement-bancaire-form.service';

describe('VirementBancaire Form Service', () => {
  let service: VirementBancaireFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VirementBancaireFormService);
  });

  describe('Service methods', () => {
    describe('createVirementBancaireFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createVirementBancaireFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateVirement: expect.any(Object),
            montant: expect.any(Object),
            compteSource: expect.any(Object),
            compteDestination: expect.any(Object),
          })
        );
      });

      it('passing IVirementBancaire should create a new form with FormGroup', () => {
        const formGroup = service.createVirementBancaireFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateVirement: expect.any(Object),
            montant: expect.any(Object),
            compteSource: expect.any(Object),
            compteDestination: expect.any(Object),
          })
        );
      });
    });

    describe('getVirementBancaire', () => {
      it('should return NewVirementBancaire for default VirementBancaire initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createVirementBancaireFormGroup(sampleWithNewData);

        const virementBancaire = service.getVirementBancaire(formGroup) as any;

        expect(virementBancaire).toMatchObject(sampleWithNewData);
      });

      it('should return NewVirementBancaire for empty VirementBancaire initial value', () => {
        const formGroup = service.createVirementBancaireFormGroup();

        const virementBancaire = service.getVirementBancaire(formGroup) as any;

        expect(virementBancaire).toMatchObject({});
      });

      it('should return IVirementBancaire', () => {
        const formGroup = service.createVirementBancaireFormGroup(sampleWithRequiredData);

        const virementBancaire = service.getVirementBancaire(formGroup) as any;

        expect(virementBancaire).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IVirementBancaire should not enable id FormControl', () => {
        const formGroup = service.createVirementBancaireFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewVirementBancaire should disable id FormControl', () => {
        const formGroup = service.createVirementBancaireFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
