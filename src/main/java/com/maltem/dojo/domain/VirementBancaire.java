package com.maltem.dojo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A VirementBancaire.
 */
@Entity
@Table(name = "virement_bancaire")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class VirementBancaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date_virement")
    private ZonedDateTime dateVirement;

    @NotNull
    @Column(name = "montant", nullable = false)
    private Double montant;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "titulaire" }, allowSetters = true)
    private CompteBancaire compteSource;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "titulaire" }, allowSetters = true)
    private CompteBancaire compteDestination;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public VirementBancaire id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateVirement() {
        return this.dateVirement;
    }

    public VirementBancaire dateVirement(ZonedDateTime dateVirement) {
        this.setDateVirement(dateVirement);
        return this;
    }

    public void setDateVirement(ZonedDateTime dateVirement) {
        this.dateVirement = dateVirement;
    }

    public Double getMontant() {
        return this.montant;
    }

    public VirementBancaire montant(Double montant) {
        this.setMontant(montant);
        return this;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public CompteBancaire getCompteSource() {
        return this.compteSource;
    }

    public void setCompteSource(CompteBancaire compteBancaire) {
        this.compteSource = compteBancaire;
    }

    public VirementBancaire compteSource(CompteBancaire compteBancaire) {
        this.setCompteSource(compteBancaire);
        return this;
    }

    public CompteBancaire getCompteDestination() {
        return this.compteDestination;
    }

    public void setCompteDestination(CompteBancaire compteBancaire) {
        this.compteDestination = compteBancaire;
    }

    public VirementBancaire compteDestination(CompteBancaire compteBancaire) {
        this.setCompteDestination(compteBancaire);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VirementBancaire)) {
            return false;
        }
        return id != null && id.equals(((VirementBancaire) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VirementBancaire{" +
            "id=" + getId() +
            ", dateVirement='" + getDateVirement() + "'" +
            ", montant=" + getMontant() +
            "}";
    }
}
