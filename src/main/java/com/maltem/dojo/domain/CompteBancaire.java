package com.maltem.dojo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A CompteBancaire.
 */
@Entity
@Table(name = "compte_bancaire")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CompteBancaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "numero", nullable = false)
    private String numero;

    @NotNull
    @Column(name = "solde", nullable = false)
    private String solde;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "user" }, allowSetters = true)
    private ClientBanque titulaire;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CompteBancaire id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return this.numero;
    }

    public CompteBancaire numero(String numero) {
        this.setNumero(numero);
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getSolde() {
        return this.solde;
    }

    public CompteBancaire solde(String solde) {
        this.setSolde(solde);
        return this;
    }

    public void setSolde(String solde) {
        this.solde = solde;
    }

    public ClientBanque getTitulaire() {
        return this.titulaire;
    }

    public void setTitulaire(ClientBanque clientBanque) {
        this.titulaire = clientBanque;
    }

    public CompteBancaire titulaire(ClientBanque clientBanque) {
        this.setTitulaire(clientBanque);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompteBancaire)) {
            return false;
        }
        return id != null && id.equals(((CompteBancaire) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompteBancaire{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", solde='" + getSolde() + "'" +
            "}";
    }
}
