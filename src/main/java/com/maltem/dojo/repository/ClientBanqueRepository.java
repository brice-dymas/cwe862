package com.maltem.dojo.repository;

import com.maltem.dojo.domain.ClientBanque;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ClientBanque entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientBanqueRepository extends JpaRepository<ClientBanque, Long> {}
