package com.maltem.dojo.repository;

import com.maltem.dojo.domain.CompteBancaire;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the CompteBancaire entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompteBancaireRepository extends JpaRepository<CompteBancaire, Long> {}
