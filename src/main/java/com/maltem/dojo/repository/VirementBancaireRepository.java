package com.maltem.dojo.repository;

import com.maltem.dojo.domain.VirementBancaire;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the VirementBancaire entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VirementBancaireRepository extends JpaRepository<VirementBancaire, Long> {}
