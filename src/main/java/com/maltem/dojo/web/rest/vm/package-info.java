/**
 * View Models used by Spring MVC REST controllers.
 */
package com.maltem.dojo.web.rest.vm;
