package com.maltem.dojo.web.rest;

import com.maltem.dojo.domain.ClientBanque;
import com.maltem.dojo.repository.ClientBanqueRepository;
import com.maltem.dojo.service.ClientBanqueService;
import com.maltem.dojo.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.maltem.dojo.domain.ClientBanque}.
 */
@RestController
@RequestMapping("/api")
public class ClientBanqueResource {

    private final Logger log = LoggerFactory.getLogger(ClientBanqueResource.class);

    private static final String ENTITY_NAME = "clientBanque";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ClientBanqueService clientBanqueService;

    private final ClientBanqueRepository clientBanqueRepository;

    public ClientBanqueResource(ClientBanqueService clientBanqueService, ClientBanqueRepository clientBanqueRepository) {
        this.clientBanqueService = clientBanqueService;
        this.clientBanqueRepository = clientBanqueRepository;
    }

    /**
     * {@code POST  /client-banques} : Create a new clientBanque.
     *
     * @param clientBanque the clientBanque to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clientBanque, or with status {@code 400 (Bad Request)} if the clientBanque has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/client-banques")
    public ResponseEntity<ClientBanque> createClientBanque(@Valid @RequestBody ClientBanque clientBanque) throws URISyntaxException {
        log.debug("REST request to save ClientBanque : {}", clientBanque);
        if (clientBanque.getId() != null) {
            throw new BadRequestAlertException("A new clientBanque cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientBanque result = clientBanqueService.save(clientBanque);
        return ResponseEntity
            .created(new URI("/api/client-banques/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /client-banques/:id} : Updates an existing clientBanque.
     *
     * @param id the id of the clientBanque to save.
     * @param clientBanque the clientBanque to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated clientBanque,
     * or with status {@code 400 (Bad Request)} if the clientBanque is not valid,
     * or with status {@code 500 (Internal Server Error)} if the clientBanque couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/client-banques/{id}")
    public ResponseEntity<ClientBanque> updateClientBanque(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ClientBanque clientBanque
    ) throws URISyntaxException {
        log.debug("REST request to update ClientBanque : {}, {}", id, clientBanque);
        if (clientBanque.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, clientBanque.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!clientBanqueRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ClientBanque result = clientBanqueService.update(clientBanque);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, clientBanque.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /client-banques/:id} : Partial updates given fields of an existing clientBanque, field will ignore if it is null
     *
     * @param id the id of the clientBanque to save.
     * @param clientBanque the clientBanque to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated clientBanque,
     * or with status {@code 400 (Bad Request)} if the clientBanque is not valid,
     * or with status {@code 404 (Not Found)} if the clientBanque is not found,
     * or with status {@code 500 (Internal Server Error)} if the clientBanque couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/client-banques/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ClientBanque> partialUpdateClientBanque(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ClientBanque clientBanque
    ) throws URISyntaxException {
        log.debug("REST request to partial update ClientBanque partially : {}, {}", id, clientBanque);
        if (clientBanque.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, clientBanque.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!clientBanqueRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ClientBanque> result = clientBanqueService.partialUpdate(clientBanque);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, clientBanque.getId().toString())
        );
    }

    /**
     * {@code GET  /client-banques} : get all the clientBanques.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of clientBanques in body.
     */
    @GetMapping("/client-banques")
    public ResponseEntity<List<ClientBanque>> getAllClientBanques(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of ClientBanques");
        Page<ClientBanque> page = clientBanqueService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /client-banques/:id} : get the "id" clientBanque.
     *
     * @param id the id of the clientBanque to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clientBanque, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/client-banques/{id}")
    public ResponseEntity<ClientBanque> getClientBanque(@PathVariable Long id) {
        log.debug("REST request to get ClientBanque : {}", id);
        Optional<ClientBanque> clientBanque = clientBanqueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(clientBanque);
    }

    /**
     * {@code DELETE  /client-banques/:id} : delete the "id" clientBanque.
     *
     * @param id the id of the clientBanque to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/client-banques/{id}")
    public ResponseEntity<Void> deleteClientBanque(@PathVariable Long id) {
        log.debug("REST request to delete ClientBanque : {}", id);
        clientBanqueService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
