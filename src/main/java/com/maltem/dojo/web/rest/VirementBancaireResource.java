package com.maltem.dojo.web.rest;

import com.maltem.dojo.domain.VirementBancaire;
import com.maltem.dojo.repository.VirementBancaireRepository;
import com.maltem.dojo.service.VirementBancaireService;
import com.maltem.dojo.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.maltem.dojo.domain.VirementBancaire}.
 */
@RestController
@RequestMapping("/api")
public class VirementBancaireResource {

    private final Logger log = LoggerFactory.getLogger(VirementBancaireResource.class);

    private static final String ENTITY_NAME = "virementBancaire";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VirementBancaireService virementBancaireService;

    private final VirementBancaireRepository virementBancaireRepository;

    public VirementBancaireResource(
        VirementBancaireService virementBancaireService,
        VirementBancaireRepository virementBancaireRepository
    ) {
        this.virementBancaireService = virementBancaireService;
        this.virementBancaireRepository = virementBancaireRepository;
    }

    /**
     * {@code POST  /virement-bancaires} : Create a new virementBancaire.
     *
     * @param virementBancaire the virementBancaire to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new virementBancaire, or with status {@code 400 (Bad Request)} if the virementBancaire has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/virement-bancaires")
    public ResponseEntity<VirementBancaire> createVirementBancaire(@Valid @RequestBody VirementBancaire virementBancaire)
        throws URISyntaxException {
        log.debug("REST request to save VirementBancaire : {}", virementBancaire);
        if (virementBancaire.getId() != null) {
            throw new BadRequestAlertException("A new virementBancaire cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VirementBancaire result = virementBancaireService.save(virementBancaire);
        return ResponseEntity
            .created(new URI("/api/virement-bancaires/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /virement-bancaires/:id} : Updates an existing virementBancaire.
     *
     * @param id the id of the virementBancaire to save.
     * @param virementBancaire the virementBancaire to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated virementBancaire,
     * or with status {@code 400 (Bad Request)} if the virementBancaire is not valid,
     * or with status {@code 500 (Internal Server Error)} if the virementBancaire couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/virement-bancaires/{id}")
    public ResponseEntity<VirementBancaire> updateVirementBancaire(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody VirementBancaire virementBancaire
    ) throws URISyntaxException {
        log.debug("REST request to update VirementBancaire : {}, {}", id, virementBancaire);
        if (virementBancaire.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, virementBancaire.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!virementBancaireRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        VirementBancaire result = virementBancaireService.update(virementBancaire);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, virementBancaire.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /virement-bancaires/:id} : Partial updates given fields of an existing virementBancaire, field will ignore if it is null
     *
     * @param id the id of the virementBancaire to save.
     * @param virementBancaire the virementBancaire to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated virementBancaire,
     * or with status {@code 400 (Bad Request)} if the virementBancaire is not valid,
     * or with status {@code 404 (Not Found)} if the virementBancaire is not found,
     * or with status {@code 500 (Internal Server Error)} if the virementBancaire couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/virement-bancaires/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<VirementBancaire> partialUpdateVirementBancaire(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody VirementBancaire virementBancaire
    ) throws URISyntaxException {
        log.debug("REST request to partial update VirementBancaire partially : {}, {}", id, virementBancaire);
        if (virementBancaire.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, virementBancaire.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!virementBancaireRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<VirementBancaire> result = virementBancaireService.partialUpdate(virementBancaire);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, virementBancaire.getId().toString())
        );
    }

    /**
     * {@code GET  /virement-bancaires} : get all the virementBancaires.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of virementBancaires in body.
     */
    @GetMapping("/virement-bancaires")
    public ResponseEntity<List<VirementBancaire>> getAllVirementBancaires(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of VirementBancaires");
        Page<VirementBancaire> page = virementBancaireService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /virement-bancaires/:id} : get the "id" virementBancaire.
     *
     * @param id the id of the virementBancaire to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the virementBancaire, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/virement-bancaires/{id}")
    public ResponseEntity<VirementBancaire> getVirementBancaire(@PathVariable Long id) {
        log.debug("REST request to get VirementBancaire : {}", id);
        Optional<VirementBancaire> virementBancaire = virementBancaireService.findOne(id);
        return ResponseUtil.wrapOrNotFound(virementBancaire);
    }

    /**
     * {@code DELETE  /virement-bancaires/:id} : delete the "id" virementBancaire.
     *
     * @param id the id of the virementBancaire to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/virement-bancaires/{id}")
    public ResponseEntity<Void> deleteVirementBancaire(@PathVariable Long id) {
        log.debug("REST request to delete VirementBancaire : {}", id);
        virementBancaireService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
