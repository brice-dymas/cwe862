package com.maltem.dojo.web.rest;

import com.maltem.dojo.domain.CompteBancaire;
import com.maltem.dojo.repository.CompteBancaireRepository;
import com.maltem.dojo.service.CompteBancaireService;
import com.maltem.dojo.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.maltem.dojo.domain.CompteBancaire}.
 */
@RestController
@RequestMapping("/api")
public class CompteBancaireResource {

    private final Logger log = LoggerFactory.getLogger(CompteBancaireResource.class);

    private static final String ENTITY_NAME = "compteBancaire";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompteBancaireService compteBancaireService;

    private final CompteBancaireRepository compteBancaireRepository;

    public CompteBancaireResource(CompteBancaireService compteBancaireService, CompteBancaireRepository compteBancaireRepository) {
        this.compteBancaireService = compteBancaireService;
        this.compteBancaireRepository = compteBancaireRepository;
    }

    /**
     * {@code POST  /compte-bancaires} : Create a new compteBancaire.
     *
     * @param compteBancaire the compteBancaire to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new compteBancaire, or with status {@code 400 (Bad Request)} if the compteBancaire has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/compte-bancaires")
    public ResponseEntity<CompteBancaire> createCompteBancaire(@Valid @RequestBody CompteBancaire compteBancaire)
        throws URISyntaxException {
        log.debug("REST request to save CompteBancaire : {}", compteBancaire);
        if (compteBancaire.getId() != null) {
            throw new BadRequestAlertException("A new compteBancaire cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CompteBancaire result = compteBancaireService.save(compteBancaire);
        return ResponseEntity
            .created(new URI("/api/compte-bancaires/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /compte-bancaires/:id} : Updates an existing compteBancaire.
     *
     * @param id the id of the compteBancaire to save.
     * @param compteBancaire the compteBancaire to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated compteBancaire,
     * or with status {@code 400 (Bad Request)} if the compteBancaire is not valid,
     * or with status {@code 500 (Internal Server Error)} if the compteBancaire couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/compte-bancaires/{id}")
    public ResponseEntity<CompteBancaire> updateCompteBancaire(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CompteBancaire compteBancaire
    ) throws URISyntaxException {
        log.debug("REST request to update CompteBancaire : {}, {}", id, compteBancaire);
        if (compteBancaire.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, compteBancaire.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!compteBancaireRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CompteBancaire result = compteBancaireService.update(compteBancaire);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, compteBancaire.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /compte-bancaires/:id} : Partial updates given fields of an existing compteBancaire, field will ignore if it is null
     *
     * @param id the id of the compteBancaire to save.
     * @param compteBancaire the compteBancaire to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated compteBancaire,
     * or with status {@code 400 (Bad Request)} if the compteBancaire is not valid,
     * or with status {@code 404 (Not Found)} if the compteBancaire is not found,
     * or with status {@code 500 (Internal Server Error)} if the compteBancaire couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/compte-bancaires/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CompteBancaire> partialUpdateCompteBancaire(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody CompteBancaire compteBancaire
    ) throws URISyntaxException {
        log.debug("REST request to partial update CompteBancaire partially : {}, {}", id, compteBancaire);
        if (compteBancaire.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, compteBancaire.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!compteBancaireRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CompteBancaire> result = compteBancaireService.partialUpdate(compteBancaire);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, compteBancaire.getId().toString())
        );
    }

    /**
     * {@code GET  /compte-bancaires} : get all the compteBancaires.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of compteBancaires in body.
     */
    @GetMapping("/compte-bancaires")
    public ResponseEntity<List<CompteBancaire>> getAllCompteBancaires(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of CompteBancaires");
        Page<CompteBancaire> page = compteBancaireService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /compte-bancaires/:id} : get the "id" compteBancaire.
     *
     * @param id the id of the compteBancaire to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the compteBancaire, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/compte-bancaires/{id}")
    public ResponseEntity<CompteBancaire> getCompteBancaire(@PathVariable Long id) {
        log.debug("REST request to get CompteBancaire : {}", id);
        Optional<CompteBancaire> compteBancaire = compteBancaireService.findOne(id);
        return ResponseUtil.wrapOrNotFound(compteBancaire);
    }

    /**
     * {@code DELETE  /compte-bancaires/:id} : delete the "id" compteBancaire.
     *
     * @param id the id of the compteBancaire to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/compte-bancaires/{id}")
    public ResponseEntity<Void> deleteCompteBancaire(@PathVariable Long id) {
        log.debug("REST request to delete CompteBancaire : {}", id);
        compteBancaireService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
