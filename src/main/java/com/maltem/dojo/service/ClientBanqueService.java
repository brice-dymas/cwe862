package com.maltem.dojo.service;

import com.maltem.dojo.domain.ClientBanque;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link ClientBanque}.
 */
public interface ClientBanqueService {
    /**
     * Save a clientBanque.
     *
     * @param clientBanque the entity to save.
     * @return the persisted entity.
     */
    ClientBanque save(ClientBanque clientBanque);

    /**
     * Updates a clientBanque.
     *
     * @param clientBanque the entity to update.
     * @return the persisted entity.
     */
    ClientBanque update(ClientBanque clientBanque);

    /**
     * Partially updates a clientBanque.
     *
     * @param clientBanque the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ClientBanque> partialUpdate(ClientBanque clientBanque);

    /**
     * Get all the clientBanques.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ClientBanque> findAll(Pageable pageable);

    /**
     * Get the "id" clientBanque.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ClientBanque> findOne(Long id);

    /**
     * Delete the "id" clientBanque.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
