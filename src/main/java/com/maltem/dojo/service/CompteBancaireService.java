package com.maltem.dojo.service;

import com.maltem.dojo.domain.CompteBancaire;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link CompteBancaire}.
 */
public interface CompteBancaireService {
    /**
     * Save a compteBancaire.
     *
     * @param compteBancaire the entity to save.
     * @return the persisted entity.
     */
    CompteBancaire save(CompteBancaire compteBancaire);

    /**
     * Updates a compteBancaire.
     *
     * @param compteBancaire the entity to update.
     * @return the persisted entity.
     */
    CompteBancaire update(CompteBancaire compteBancaire);

    /**
     * Partially updates a compteBancaire.
     *
     * @param compteBancaire the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CompteBancaire> partialUpdate(CompteBancaire compteBancaire);

    /**
     * Get all the compteBancaires.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CompteBancaire> findAll(Pageable pageable);

    /**
     * Get the "id" compteBancaire.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CompteBancaire> findOne(Long id);

    /**
     * Delete the "id" compteBancaire.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
