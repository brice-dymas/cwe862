package com.maltem.dojo.service.impl;

import com.maltem.dojo.domain.CompteBancaire;
import com.maltem.dojo.repository.CompteBancaireRepository;
import com.maltem.dojo.service.CompteBancaireService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link CompteBancaire}.
 */
@Service
@Transactional
public class CompteBancaireServiceImpl implements CompteBancaireService {

    private final Logger log = LoggerFactory.getLogger(CompteBancaireServiceImpl.class);

    private final CompteBancaireRepository compteBancaireRepository;

    public CompteBancaireServiceImpl(CompteBancaireRepository compteBancaireRepository) {
        this.compteBancaireRepository = compteBancaireRepository;
    }

    @Override
    public CompteBancaire save(CompteBancaire compteBancaire) {
        log.debug("Request to save CompteBancaire : {}", compteBancaire);
        return compteBancaireRepository.save(compteBancaire);
    }

    @Override
    public CompteBancaire update(CompteBancaire compteBancaire) {
        log.debug("Request to update CompteBancaire : {}", compteBancaire);
        return compteBancaireRepository.save(compteBancaire);
    }

    @Override
    public Optional<CompteBancaire> partialUpdate(CompteBancaire compteBancaire) {
        log.debug("Request to partially update CompteBancaire : {}", compteBancaire);

        return compteBancaireRepository
            .findById(compteBancaire.getId())
            .map(existingCompteBancaire -> {
                if (compteBancaire.getNumero() != null) {
                    existingCompteBancaire.setNumero(compteBancaire.getNumero());
                }
                if (compteBancaire.getSolde() != null) {
                    existingCompteBancaire.setSolde(compteBancaire.getSolde());
                }

                return existingCompteBancaire;
            })
            .map(compteBancaireRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CompteBancaire> findAll(Pageable pageable) {
        log.debug("Request to get all CompteBancaires");
        return compteBancaireRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CompteBancaire> findOne(Long id) {
        log.debug("Request to get CompteBancaire : {}", id);
        return compteBancaireRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CompteBancaire : {}", id);
        compteBancaireRepository.deleteById(id);
    }
}
