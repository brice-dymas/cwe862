package com.maltem.dojo.service.impl;

import com.maltem.dojo.domain.ClientBanque;
import com.maltem.dojo.repository.ClientBanqueRepository;
import com.maltem.dojo.service.ClientBanqueService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ClientBanque}.
 */
@Service
@Transactional
public class ClientBanqueServiceImpl implements ClientBanqueService {

    private final Logger log = LoggerFactory.getLogger(ClientBanqueServiceImpl.class);

    private final ClientBanqueRepository clientBanqueRepository;

    public ClientBanqueServiceImpl(ClientBanqueRepository clientBanqueRepository) {
        this.clientBanqueRepository = clientBanqueRepository;
    }

    @Override
    public ClientBanque save(ClientBanque clientBanque) {
        log.debug("Request to save ClientBanque : {}", clientBanque);
        return clientBanqueRepository.save(clientBanque);
    }

    @Override
    public ClientBanque update(ClientBanque clientBanque) {
        log.debug("Request to update ClientBanque : {}", clientBanque);
        return clientBanqueRepository.save(clientBanque);
    }

    @Override
    public Optional<ClientBanque> partialUpdate(ClientBanque clientBanque) {
        log.debug("Request to partially update ClientBanque : {}", clientBanque);

        return clientBanqueRepository
            .findById(clientBanque.getId())
            .map(existingClientBanque -> {
                if (clientBanque.getFirstName() != null) {
                    existingClientBanque.setFirstName(clientBanque.getFirstName());
                }
                if (clientBanque.getLastName() != null) {
                    existingClientBanque.setLastName(clientBanque.getLastName());
                }

                return existingClientBanque;
            })
            .map(clientBanqueRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ClientBanque> findAll(Pageable pageable) {
        log.debug("Request to get all ClientBanques");
        return clientBanqueRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ClientBanque> findOne(Long id) {
        log.debug("Request to get ClientBanque : {}", id);
        return clientBanqueRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ClientBanque : {}", id);
        clientBanqueRepository.deleteById(id);
    }
}
