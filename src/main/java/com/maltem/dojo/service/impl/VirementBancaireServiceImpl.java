package com.maltem.dojo.service.impl;

import com.maltem.dojo.domain.VirementBancaire;
import com.maltem.dojo.repository.VirementBancaireRepository;
import com.maltem.dojo.service.VirementBancaireService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link VirementBancaire}.
 */
@Service
@Transactional
public class VirementBancaireServiceImpl implements VirementBancaireService {

    private final Logger log = LoggerFactory.getLogger(VirementBancaireServiceImpl.class);

    private final VirementBancaireRepository virementBancaireRepository;

    public VirementBancaireServiceImpl(VirementBancaireRepository virementBancaireRepository) {
        this.virementBancaireRepository = virementBancaireRepository;
    }

    @Override
    public VirementBancaire save(VirementBancaire virementBancaire) {
        log.debug("Request to save VirementBancaire : {}", virementBancaire);
        return virementBancaireRepository.save(virementBancaire);
    }

    @Override
    public VirementBancaire update(VirementBancaire virementBancaire) {
        log.debug("Request to update VirementBancaire : {}", virementBancaire);
        return virementBancaireRepository.save(virementBancaire);
    }

    @Override
    public Optional<VirementBancaire> partialUpdate(VirementBancaire virementBancaire) {
        log.debug("Request to partially update VirementBancaire : {}", virementBancaire);

        return virementBancaireRepository
            .findById(virementBancaire.getId())
            .map(existingVirementBancaire -> {
                if (virementBancaire.getDateVirement() != null) {
                    existingVirementBancaire.setDateVirement(virementBancaire.getDateVirement());
                }
                if (virementBancaire.getMontant() != null) {
                    existingVirementBancaire.setMontant(virementBancaire.getMontant());
                }

                return existingVirementBancaire;
            })
            .map(virementBancaireRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<VirementBancaire> findAll(Pageable pageable) {
        log.debug("Request to get all VirementBancaires");
        return virementBancaireRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<VirementBancaire> findOne(Long id) {
        log.debug("Request to get VirementBancaire : {}", id);
        return virementBancaireRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete VirementBancaire : {}", id);
        virementBancaireRepository.deleteById(id);
    }
}
