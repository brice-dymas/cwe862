package com.maltem.dojo.service;

import com.maltem.dojo.domain.VirementBancaire;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link VirementBancaire}.
 */
public interface VirementBancaireService {
    /**
     * Save a virementBancaire.
     *
     * @param virementBancaire the entity to save.
     * @return the persisted entity.
     */
    VirementBancaire save(VirementBancaire virementBancaire);

    /**
     * Updates a virementBancaire.
     *
     * @param virementBancaire the entity to update.
     * @return the persisted entity.
     */
    VirementBancaire update(VirementBancaire virementBancaire);

    /**
     * Partially updates a virementBancaire.
     *
     * @param virementBancaire the entity to update partially.
     * @return the persisted entity.
     */
    Optional<VirementBancaire> partialUpdate(VirementBancaire virementBancaire);

    /**
     * Get all the virementBancaires.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<VirementBancaire> findAll(Pageable pageable);

    /**
     * Get the "id" virementBancaire.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<VirementBancaire> findOne(Long id);

    /**
     * Delete the "id" virementBancaire.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
