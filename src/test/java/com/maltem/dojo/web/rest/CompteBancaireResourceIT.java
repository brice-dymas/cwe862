package com.maltem.dojo.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.maltem.dojo.IntegrationTest;
import com.maltem.dojo.domain.ClientBanque;
import com.maltem.dojo.domain.CompteBancaire;
import com.maltem.dojo.repository.CompteBancaireRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CompteBancaireResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CompteBancaireResourceIT {

    private static final String DEFAULT_NUMERO = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO = "BBBBBBBBBB";

    private static final String DEFAULT_SOLDE = "AAAAAAAAAA";
    private static final String UPDATED_SOLDE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/compte-bancaires";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CompteBancaireRepository compteBancaireRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompteBancaireMockMvc;

    private CompteBancaire compteBancaire;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompteBancaire createEntity(EntityManager em) {
        CompteBancaire compteBancaire = new CompteBancaire().numero(DEFAULT_NUMERO).solde(DEFAULT_SOLDE);
        // Add required entity
        ClientBanque clientBanque;
        if (TestUtil.findAll(em, ClientBanque.class).isEmpty()) {
            clientBanque = ClientBanqueResourceIT.createEntity(em);
            em.persist(clientBanque);
            em.flush();
        } else {
            clientBanque = TestUtil.findAll(em, ClientBanque.class).get(0);
        }
        compteBancaire.setTitulaire(clientBanque);
        return compteBancaire;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompteBancaire createUpdatedEntity(EntityManager em) {
        CompteBancaire compteBancaire = new CompteBancaire().numero(UPDATED_NUMERO).solde(UPDATED_SOLDE);
        // Add required entity
        ClientBanque clientBanque;
        if (TestUtil.findAll(em, ClientBanque.class).isEmpty()) {
            clientBanque = ClientBanqueResourceIT.createUpdatedEntity(em);
            em.persist(clientBanque);
            em.flush();
        } else {
            clientBanque = TestUtil.findAll(em, ClientBanque.class).get(0);
        }
        compteBancaire.setTitulaire(clientBanque);
        return compteBancaire;
    }

    @BeforeEach
    public void initTest() {
        compteBancaire = createEntity(em);
    }

    @Test
    @Transactional
    void createCompteBancaire() throws Exception {
        int databaseSizeBeforeCreate = compteBancaireRepository.findAll().size();
        // Create the CompteBancaire
        restCompteBancaireMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(compteBancaire))
            )
            .andExpect(status().isCreated());

        // Validate the CompteBancaire in the database
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeCreate + 1);
        CompteBancaire testCompteBancaire = compteBancaireList.get(compteBancaireList.size() - 1);
        assertThat(testCompteBancaire.getNumero()).isEqualTo(DEFAULT_NUMERO);
        assertThat(testCompteBancaire.getSolde()).isEqualTo(DEFAULT_SOLDE);
    }

    @Test
    @Transactional
    void createCompteBancaireWithExistingId() throws Exception {
        // Create the CompteBancaire with an existing ID
        compteBancaire.setId(1L);

        int databaseSizeBeforeCreate = compteBancaireRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompteBancaireMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(compteBancaire))
            )
            .andExpect(status().isBadRequest());

        // Validate the CompteBancaire in the database
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNumeroIsRequired() throws Exception {
        int databaseSizeBeforeTest = compteBancaireRepository.findAll().size();
        // set the field null
        compteBancaire.setNumero(null);

        // Create the CompteBancaire, which fails.

        restCompteBancaireMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(compteBancaire))
            )
            .andExpect(status().isBadRequest());

        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSoldeIsRequired() throws Exception {
        int databaseSizeBeforeTest = compteBancaireRepository.findAll().size();
        // set the field null
        compteBancaire.setSolde(null);

        // Create the CompteBancaire, which fails.

        restCompteBancaireMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(compteBancaire))
            )
            .andExpect(status().isBadRequest());

        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCompteBancaires() throws Exception {
        // Initialize the database
        compteBancaireRepository.saveAndFlush(compteBancaire);

        // Get all the compteBancaireList
        restCompteBancaireMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(compteBancaire.getId().intValue())))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO)))
            .andExpect(jsonPath("$.[*].solde").value(hasItem(DEFAULT_SOLDE)));
    }

    @Test
    @Transactional
    void getCompteBancaire() throws Exception {
        // Initialize the database
        compteBancaireRepository.saveAndFlush(compteBancaire);

        // Get the compteBancaire
        restCompteBancaireMockMvc
            .perform(get(ENTITY_API_URL_ID, compteBancaire.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(compteBancaire.getId().intValue()))
            .andExpect(jsonPath("$.numero").value(DEFAULT_NUMERO))
            .andExpect(jsonPath("$.solde").value(DEFAULT_SOLDE));
    }

    @Test
    @Transactional
    void getNonExistingCompteBancaire() throws Exception {
        // Get the compteBancaire
        restCompteBancaireMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingCompteBancaire() throws Exception {
        // Initialize the database
        compteBancaireRepository.saveAndFlush(compteBancaire);

        int databaseSizeBeforeUpdate = compteBancaireRepository.findAll().size();

        // Update the compteBancaire
        CompteBancaire updatedCompteBancaire = compteBancaireRepository.findById(compteBancaire.getId()).get();
        // Disconnect from session so that the updates on updatedCompteBancaire are not directly saved in db
        em.detach(updatedCompteBancaire);
        updatedCompteBancaire.numero(UPDATED_NUMERO).solde(UPDATED_SOLDE);

        restCompteBancaireMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCompteBancaire.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCompteBancaire))
            )
            .andExpect(status().isOk());

        // Validate the CompteBancaire in the database
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeUpdate);
        CompteBancaire testCompteBancaire = compteBancaireList.get(compteBancaireList.size() - 1);
        assertThat(testCompteBancaire.getNumero()).isEqualTo(UPDATED_NUMERO);
        assertThat(testCompteBancaire.getSolde()).isEqualTo(UPDATED_SOLDE);
    }

    @Test
    @Transactional
    void putNonExistingCompteBancaire() throws Exception {
        int databaseSizeBeforeUpdate = compteBancaireRepository.findAll().size();
        compteBancaire.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompteBancaireMockMvc
            .perform(
                put(ENTITY_API_URL_ID, compteBancaire.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(compteBancaire))
            )
            .andExpect(status().isBadRequest());

        // Validate the CompteBancaire in the database
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCompteBancaire() throws Exception {
        int databaseSizeBeforeUpdate = compteBancaireRepository.findAll().size();
        compteBancaire.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCompteBancaireMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(compteBancaire))
            )
            .andExpect(status().isBadRequest());

        // Validate the CompteBancaire in the database
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCompteBancaire() throws Exception {
        int databaseSizeBeforeUpdate = compteBancaireRepository.findAll().size();
        compteBancaire.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCompteBancaireMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(compteBancaire)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the CompteBancaire in the database
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCompteBancaireWithPatch() throws Exception {
        // Initialize the database
        compteBancaireRepository.saveAndFlush(compteBancaire);

        int databaseSizeBeforeUpdate = compteBancaireRepository.findAll().size();

        // Update the compteBancaire using partial update
        CompteBancaire partialUpdatedCompteBancaire = new CompteBancaire();
        partialUpdatedCompteBancaire.setId(compteBancaire.getId());

        partialUpdatedCompteBancaire.numero(UPDATED_NUMERO);

        restCompteBancaireMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCompteBancaire.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCompteBancaire))
            )
            .andExpect(status().isOk());

        // Validate the CompteBancaire in the database
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeUpdate);
        CompteBancaire testCompteBancaire = compteBancaireList.get(compteBancaireList.size() - 1);
        assertThat(testCompteBancaire.getNumero()).isEqualTo(UPDATED_NUMERO);
        assertThat(testCompteBancaire.getSolde()).isEqualTo(DEFAULT_SOLDE);
    }

    @Test
    @Transactional
    void fullUpdateCompteBancaireWithPatch() throws Exception {
        // Initialize the database
        compteBancaireRepository.saveAndFlush(compteBancaire);

        int databaseSizeBeforeUpdate = compteBancaireRepository.findAll().size();

        // Update the compteBancaire using partial update
        CompteBancaire partialUpdatedCompteBancaire = new CompteBancaire();
        partialUpdatedCompteBancaire.setId(compteBancaire.getId());

        partialUpdatedCompteBancaire.numero(UPDATED_NUMERO).solde(UPDATED_SOLDE);

        restCompteBancaireMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCompteBancaire.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCompteBancaire))
            )
            .andExpect(status().isOk());

        // Validate the CompteBancaire in the database
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeUpdate);
        CompteBancaire testCompteBancaire = compteBancaireList.get(compteBancaireList.size() - 1);
        assertThat(testCompteBancaire.getNumero()).isEqualTo(UPDATED_NUMERO);
        assertThat(testCompteBancaire.getSolde()).isEqualTo(UPDATED_SOLDE);
    }

    @Test
    @Transactional
    void patchNonExistingCompteBancaire() throws Exception {
        int databaseSizeBeforeUpdate = compteBancaireRepository.findAll().size();
        compteBancaire.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompteBancaireMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, compteBancaire.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(compteBancaire))
            )
            .andExpect(status().isBadRequest());

        // Validate the CompteBancaire in the database
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCompteBancaire() throws Exception {
        int databaseSizeBeforeUpdate = compteBancaireRepository.findAll().size();
        compteBancaire.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCompteBancaireMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(compteBancaire))
            )
            .andExpect(status().isBadRequest());

        // Validate the CompteBancaire in the database
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCompteBancaire() throws Exception {
        int databaseSizeBeforeUpdate = compteBancaireRepository.findAll().size();
        compteBancaire.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCompteBancaireMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(compteBancaire))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CompteBancaire in the database
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCompteBancaire() throws Exception {
        // Initialize the database
        compteBancaireRepository.saveAndFlush(compteBancaire);

        int databaseSizeBeforeDelete = compteBancaireRepository.findAll().size();

        // Delete the compteBancaire
        restCompteBancaireMockMvc
            .perform(delete(ENTITY_API_URL_ID, compteBancaire.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompteBancaire> compteBancaireList = compteBancaireRepository.findAll();
        assertThat(compteBancaireList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
