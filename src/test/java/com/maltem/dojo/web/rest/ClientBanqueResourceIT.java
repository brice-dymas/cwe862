package com.maltem.dojo.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.maltem.dojo.IntegrationTest;
import com.maltem.dojo.domain.ClientBanque;
import com.maltem.dojo.repository.ClientBanqueRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ClientBanqueResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ClientBanqueResourceIT {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/client-banques";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ClientBanqueRepository clientBanqueRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restClientBanqueMockMvc;

    private ClientBanque clientBanque;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientBanque createEntity(EntityManager em) {
        ClientBanque clientBanque = new ClientBanque().firstName(DEFAULT_FIRST_NAME).lastName(DEFAULT_LAST_NAME);
        return clientBanque;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientBanque createUpdatedEntity(EntityManager em) {
        ClientBanque clientBanque = new ClientBanque().firstName(UPDATED_FIRST_NAME).lastName(UPDATED_LAST_NAME);
        return clientBanque;
    }

    @BeforeEach
    public void initTest() {
        clientBanque = createEntity(em);
    }

    @Test
    @Transactional
    void createClientBanque() throws Exception {
        int databaseSizeBeforeCreate = clientBanqueRepository.findAll().size();
        // Create the ClientBanque
        restClientBanqueMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clientBanque)))
            .andExpect(status().isCreated());

        // Validate the ClientBanque in the database
        List<ClientBanque> clientBanqueList = clientBanqueRepository.findAll();
        assertThat(clientBanqueList).hasSize(databaseSizeBeforeCreate + 1);
        ClientBanque testClientBanque = clientBanqueList.get(clientBanqueList.size() - 1);
        assertThat(testClientBanque.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testClientBanque.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
    }

    @Test
    @Transactional
    void createClientBanqueWithExistingId() throws Exception {
        // Create the ClientBanque with an existing ID
        clientBanque.setId(1L);

        int databaseSizeBeforeCreate = clientBanqueRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientBanqueMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clientBanque)))
            .andExpect(status().isBadRequest());

        // Validate the ClientBanque in the database
        List<ClientBanque> clientBanqueList = clientBanqueRepository.findAll();
        assertThat(clientBanqueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkFirstNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientBanqueRepository.findAll().size();
        // set the field null
        clientBanque.setFirstName(null);

        // Create the ClientBanque, which fails.

        restClientBanqueMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clientBanque)))
            .andExpect(status().isBadRequest());

        List<ClientBanque> clientBanqueList = clientBanqueRepository.findAll();
        assertThat(clientBanqueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientBanqueRepository.findAll().size();
        // set the field null
        clientBanque.setLastName(null);

        // Create the ClientBanque, which fails.

        restClientBanqueMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clientBanque)))
            .andExpect(status().isBadRequest());

        List<ClientBanque> clientBanqueList = clientBanqueRepository.findAll();
        assertThat(clientBanqueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllClientBanques() throws Exception {
        // Initialize the database
        clientBanqueRepository.saveAndFlush(clientBanque);

        // Get all the clientBanqueList
        restClientBanqueMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientBanque.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)));
    }

    @Test
    @Transactional
    void getClientBanque() throws Exception {
        // Initialize the database
        clientBanqueRepository.saveAndFlush(clientBanque);

        // Get the clientBanque
        restClientBanqueMockMvc
            .perform(get(ENTITY_API_URL_ID, clientBanque.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(clientBanque.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME));
    }

    @Test
    @Transactional
    void getNonExistingClientBanque() throws Exception {
        // Get the clientBanque
        restClientBanqueMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingClientBanque() throws Exception {
        // Initialize the database
        clientBanqueRepository.saveAndFlush(clientBanque);

        int databaseSizeBeforeUpdate = clientBanqueRepository.findAll().size();

        // Update the clientBanque
        ClientBanque updatedClientBanque = clientBanqueRepository.findById(clientBanque.getId()).get();
        // Disconnect from session so that the updates on updatedClientBanque are not directly saved in db
        em.detach(updatedClientBanque);
        updatedClientBanque.firstName(UPDATED_FIRST_NAME).lastName(UPDATED_LAST_NAME);

        restClientBanqueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedClientBanque.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedClientBanque))
            )
            .andExpect(status().isOk());

        // Validate the ClientBanque in the database
        List<ClientBanque> clientBanqueList = clientBanqueRepository.findAll();
        assertThat(clientBanqueList).hasSize(databaseSizeBeforeUpdate);
        ClientBanque testClientBanque = clientBanqueList.get(clientBanqueList.size() - 1);
        assertThat(testClientBanque.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testClientBanque.getLastName()).isEqualTo(UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    void putNonExistingClientBanque() throws Exception {
        int databaseSizeBeforeUpdate = clientBanqueRepository.findAll().size();
        clientBanque.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClientBanqueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, clientBanque.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(clientBanque))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClientBanque in the database
        List<ClientBanque> clientBanqueList = clientBanqueRepository.findAll();
        assertThat(clientBanqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchClientBanque() throws Exception {
        int databaseSizeBeforeUpdate = clientBanqueRepository.findAll().size();
        clientBanque.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClientBanqueMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(clientBanque))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClientBanque in the database
        List<ClientBanque> clientBanqueList = clientBanqueRepository.findAll();
        assertThat(clientBanqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamClientBanque() throws Exception {
        int databaseSizeBeforeUpdate = clientBanqueRepository.findAll().size();
        clientBanque.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClientBanqueMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clientBanque)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ClientBanque in the database
        List<ClientBanque> clientBanqueList = clientBanqueRepository.findAll();
        assertThat(clientBanqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateClientBanqueWithPatch() throws Exception {
        // Initialize the database
        clientBanqueRepository.saveAndFlush(clientBanque);

        int databaseSizeBeforeUpdate = clientBanqueRepository.findAll().size();

        // Update the clientBanque using partial update
        ClientBanque partialUpdatedClientBanque = new ClientBanque();
        partialUpdatedClientBanque.setId(clientBanque.getId());

        restClientBanqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedClientBanque.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedClientBanque))
            )
            .andExpect(status().isOk());

        // Validate the ClientBanque in the database
        List<ClientBanque> clientBanqueList = clientBanqueRepository.findAll();
        assertThat(clientBanqueList).hasSize(databaseSizeBeforeUpdate);
        ClientBanque testClientBanque = clientBanqueList.get(clientBanqueList.size() - 1);
        assertThat(testClientBanque.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testClientBanque.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
    }

    @Test
    @Transactional
    void fullUpdateClientBanqueWithPatch() throws Exception {
        // Initialize the database
        clientBanqueRepository.saveAndFlush(clientBanque);

        int databaseSizeBeforeUpdate = clientBanqueRepository.findAll().size();

        // Update the clientBanque using partial update
        ClientBanque partialUpdatedClientBanque = new ClientBanque();
        partialUpdatedClientBanque.setId(clientBanque.getId());

        partialUpdatedClientBanque.firstName(UPDATED_FIRST_NAME).lastName(UPDATED_LAST_NAME);

        restClientBanqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedClientBanque.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedClientBanque))
            )
            .andExpect(status().isOk());

        // Validate the ClientBanque in the database
        List<ClientBanque> clientBanqueList = clientBanqueRepository.findAll();
        assertThat(clientBanqueList).hasSize(databaseSizeBeforeUpdate);
        ClientBanque testClientBanque = clientBanqueList.get(clientBanqueList.size() - 1);
        assertThat(testClientBanque.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testClientBanque.getLastName()).isEqualTo(UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingClientBanque() throws Exception {
        int databaseSizeBeforeUpdate = clientBanqueRepository.findAll().size();
        clientBanque.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClientBanqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, clientBanque.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(clientBanque))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClientBanque in the database
        List<ClientBanque> clientBanqueList = clientBanqueRepository.findAll();
        assertThat(clientBanqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchClientBanque() throws Exception {
        int databaseSizeBeforeUpdate = clientBanqueRepository.findAll().size();
        clientBanque.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClientBanqueMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(clientBanque))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClientBanque in the database
        List<ClientBanque> clientBanqueList = clientBanqueRepository.findAll();
        assertThat(clientBanqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamClientBanque() throws Exception {
        int databaseSizeBeforeUpdate = clientBanqueRepository.findAll().size();
        clientBanque.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClientBanqueMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(clientBanque))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ClientBanque in the database
        List<ClientBanque> clientBanqueList = clientBanqueRepository.findAll();
        assertThat(clientBanqueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteClientBanque() throws Exception {
        // Initialize the database
        clientBanqueRepository.saveAndFlush(clientBanque);

        int databaseSizeBeforeDelete = clientBanqueRepository.findAll().size();

        // Delete the clientBanque
        restClientBanqueMockMvc
            .perform(delete(ENTITY_API_URL_ID, clientBanque.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ClientBanque> clientBanqueList = clientBanqueRepository.findAll();
        assertThat(clientBanqueList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
