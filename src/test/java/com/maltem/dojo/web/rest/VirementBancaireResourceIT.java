package com.maltem.dojo.web.rest;

import static com.maltem.dojo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.maltem.dojo.IntegrationTest;
import com.maltem.dojo.domain.CompteBancaire;
import com.maltem.dojo.domain.VirementBancaire;
import com.maltem.dojo.repository.VirementBancaireRepository;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link VirementBancaireResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class VirementBancaireResourceIT {

    private static final ZonedDateTime DEFAULT_DATE_VIREMENT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_VIREMENT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Double DEFAULT_MONTANT = 1D;
    private static final Double UPDATED_MONTANT = 2D;

    private static final String ENTITY_API_URL = "/api/virement-bancaires";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private VirementBancaireRepository virementBancaireRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVirementBancaireMockMvc;

    private VirementBancaire virementBancaire;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VirementBancaire createEntity(EntityManager em) {
        VirementBancaire virementBancaire = new VirementBancaire().dateVirement(DEFAULT_DATE_VIREMENT).montant(DEFAULT_MONTANT);
        // Add required entity
        CompteBancaire compteBancaire;
        if (TestUtil.findAll(em, CompteBancaire.class).isEmpty()) {
            compteBancaire = CompteBancaireResourceIT.createEntity(em);
            em.persist(compteBancaire);
            em.flush();
        } else {
            compteBancaire = TestUtil.findAll(em, CompteBancaire.class).get(0);
        }
        virementBancaire.setCompteSource(compteBancaire);
        // Add required entity
        virementBancaire.setCompteDestination(compteBancaire);
        return virementBancaire;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VirementBancaire createUpdatedEntity(EntityManager em) {
        VirementBancaire virementBancaire = new VirementBancaire().dateVirement(UPDATED_DATE_VIREMENT).montant(UPDATED_MONTANT);
        // Add required entity
        CompteBancaire compteBancaire;
        if (TestUtil.findAll(em, CompteBancaire.class).isEmpty()) {
            compteBancaire = CompteBancaireResourceIT.createUpdatedEntity(em);
            em.persist(compteBancaire);
            em.flush();
        } else {
            compteBancaire = TestUtil.findAll(em, CompteBancaire.class).get(0);
        }
        virementBancaire.setCompteSource(compteBancaire);
        // Add required entity
        virementBancaire.setCompteDestination(compteBancaire);
        return virementBancaire;
    }

    @BeforeEach
    public void initTest() {
        virementBancaire = createEntity(em);
    }

    @Test
    @Transactional
    void createVirementBancaire() throws Exception {
        int databaseSizeBeforeCreate = virementBancaireRepository.findAll().size();
        // Create the VirementBancaire
        restVirementBancaireMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(virementBancaire))
            )
            .andExpect(status().isCreated());

        // Validate the VirementBancaire in the database
        List<VirementBancaire> virementBancaireList = virementBancaireRepository.findAll();
        assertThat(virementBancaireList).hasSize(databaseSizeBeforeCreate + 1);
        VirementBancaire testVirementBancaire = virementBancaireList.get(virementBancaireList.size() - 1);
        assertThat(testVirementBancaire.getDateVirement()).isEqualTo(DEFAULT_DATE_VIREMENT);
        assertThat(testVirementBancaire.getMontant()).isEqualTo(DEFAULT_MONTANT);
    }

    @Test
    @Transactional
    void createVirementBancaireWithExistingId() throws Exception {
        // Create the VirementBancaire with an existing ID
        virementBancaire.setId(1L);

        int databaseSizeBeforeCreate = virementBancaireRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restVirementBancaireMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(virementBancaire))
            )
            .andExpect(status().isBadRequest());

        // Validate the VirementBancaire in the database
        List<VirementBancaire> virementBancaireList = virementBancaireRepository.findAll();
        assertThat(virementBancaireList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkMontantIsRequired() throws Exception {
        int databaseSizeBeforeTest = virementBancaireRepository.findAll().size();
        // set the field null
        virementBancaire.setMontant(null);

        // Create the VirementBancaire, which fails.

        restVirementBancaireMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(virementBancaire))
            )
            .andExpect(status().isBadRequest());

        List<VirementBancaire> virementBancaireList = virementBancaireRepository.findAll();
        assertThat(virementBancaireList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllVirementBancaires() throws Exception {
        // Initialize the database
        virementBancaireRepository.saveAndFlush(virementBancaire);

        // Get all the virementBancaireList
        restVirementBancaireMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(virementBancaire.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateVirement").value(hasItem(sameInstant(DEFAULT_DATE_VIREMENT))))
            .andExpect(jsonPath("$.[*].montant").value(hasItem(DEFAULT_MONTANT.doubleValue())));
    }

    @Test
    @Transactional
    void getVirementBancaire() throws Exception {
        // Initialize the database
        virementBancaireRepository.saveAndFlush(virementBancaire);

        // Get the virementBancaire
        restVirementBancaireMockMvc
            .perform(get(ENTITY_API_URL_ID, virementBancaire.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(virementBancaire.getId().intValue()))
            .andExpect(jsonPath("$.dateVirement").value(sameInstant(DEFAULT_DATE_VIREMENT)))
            .andExpect(jsonPath("$.montant").value(DEFAULT_MONTANT.doubleValue()));
    }

    @Test
    @Transactional
    void getNonExistingVirementBancaire() throws Exception {
        // Get the virementBancaire
        restVirementBancaireMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingVirementBancaire() throws Exception {
        // Initialize the database
        virementBancaireRepository.saveAndFlush(virementBancaire);

        int databaseSizeBeforeUpdate = virementBancaireRepository.findAll().size();

        // Update the virementBancaire
        VirementBancaire updatedVirementBancaire = virementBancaireRepository.findById(virementBancaire.getId()).get();
        // Disconnect from session so that the updates on updatedVirementBancaire are not directly saved in db
        em.detach(updatedVirementBancaire);
        updatedVirementBancaire.dateVirement(UPDATED_DATE_VIREMENT).montant(UPDATED_MONTANT);

        restVirementBancaireMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedVirementBancaire.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedVirementBancaire))
            )
            .andExpect(status().isOk());

        // Validate the VirementBancaire in the database
        List<VirementBancaire> virementBancaireList = virementBancaireRepository.findAll();
        assertThat(virementBancaireList).hasSize(databaseSizeBeforeUpdate);
        VirementBancaire testVirementBancaire = virementBancaireList.get(virementBancaireList.size() - 1);
        assertThat(testVirementBancaire.getDateVirement()).isEqualTo(UPDATED_DATE_VIREMENT);
        assertThat(testVirementBancaire.getMontant()).isEqualTo(UPDATED_MONTANT);
    }

    @Test
    @Transactional
    void putNonExistingVirementBancaire() throws Exception {
        int databaseSizeBeforeUpdate = virementBancaireRepository.findAll().size();
        virementBancaire.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVirementBancaireMockMvc
            .perform(
                put(ENTITY_API_URL_ID, virementBancaire.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(virementBancaire))
            )
            .andExpect(status().isBadRequest());

        // Validate the VirementBancaire in the database
        List<VirementBancaire> virementBancaireList = virementBancaireRepository.findAll();
        assertThat(virementBancaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchVirementBancaire() throws Exception {
        int databaseSizeBeforeUpdate = virementBancaireRepository.findAll().size();
        virementBancaire.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVirementBancaireMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(virementBancaire))
            )
            .andExpect(status().isBadRequest());

        // Validate the VirementBancaire in the database
        List<VirementBancaire> virementBancaireList = virementBancaireRepository.findAll();
        assertThat(virementBancaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamVirementBancaire() throws Exception {
        int databaseSizeBeforeUpdate = virementBancaireRepository.findAll().size();
        virementBancaire.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVirementBancaireMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(virementBancaire))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the VirementBancaire in the database
        List<VirementBancaire> virementBancaireList = virementBancaireRepository.findAll();
        assertThat(virementBancaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateVirementBancaireWithPatch() throws Exception {
        // Initialize the database
        virementBancaireRepository.saveAndFlush(virementBancaire);

        int databaseSizeBeforeUpdate = virementBancaireRepository.findAll().size();

        // Update the virementBancaire using partial update
        VirementBancaire partialUpdatedVirementBancaire = new VirementBancaire();
        partialUpdatedVirementBancaire.setId(virementBancaire.getId());

        partialUpdatedVirementBancaire.dateVirement(UPDATED_DATE_VIREMENT).montant(UPDATED_MONTANT);

        restVirementBancaireMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedVirementBancaire.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVirementBancaire))
            )
            .andExpect(status().isOk());

        // Validate the VirementBancaire in the database
        List<VirementBancaire> virementBancaireList = virementBancaireRepository.findAll();
        assertThat(virementBancaireList).hasSize(databaseSizeBeforeUpdate);
        VirementBancaire testVirementBancaire = virementBancaireList.get(virementBancaireList.size() - 1);
        assertThat(testVirementBancaire.getDateVirement()).isEqualTo(UPDATED_DATE_VIREMENT);
        assertThat(testVirementBancaire.getMontant()).isEqualTo(UPDATED_MONTANT);
    }

    @Test
    @Transactional
    void fullUpdateVirementBancaireWithPatch() throws Exception {
        // Initialize the database
        virementBancaireRepository.saveAndFlush(virementBancaire);

        int databaseSizeBeforeUpdate = virementBancaireRepository.findAll().size();

        // Update the virementBancaire using partial update
        VirementBancaire partialUpdatedVirementBancaire = new VirementBancaire();
        partialUpdatedVirementBancaire.setId(virementBancaire.getId());

        partialUpdatedVirementBancaire.dateVirement(UPDATED_DATE_VIREMENT).montant(UPDATED_MONTANT);

        restVirementBancaireMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedVirementBancaire.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedVirementBancaire))
            )
            .andExpect(status().isOk());

        // Validate the VirementBancaire in the database
        List<VirementBancaire> virementBancaireList = virementBancaireRepository.findAll();
        assertThat(virementBancaireList).hasSize(databaseSizeBeforeUpdate);
        VirementBancaire testVirementBancaire = virementBancaireList.get(virementBancaireList.size() - 1);
        assertThat(testVirementBancaire.getDateVirement()).isEqualTo(UPDATED_DATE_VIREMENT);
        assertThat(testVirementBancaire.getMontant()).isEqualTo(UPDATED_MONTANT);
    }

    @Test
    @Transactional
    void patchNonExistingVirementBancaire() throws Exception {
        int databaseSizeBeforeUpdate = virementBancaireRepository.findAll().size();
        virementBancaire.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVirementBancaireMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, virementBancaire.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(virementBancaire))
            )
            .andExpect(status().isBadRequest());

        // Validate the VirementBancaire in the database
        List<VirementBancaire> virementBancaireList = virementBancaireRepository.findAll();
        assertThat(virementBancaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchVirementBancaire() throws Exception {
        int databaseSizeBeforeUpdate = virementBancaireRepository.findAll().size();
        virementBancaire.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVirementBancaireMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(virementBancaire))
            )
            .andExpect(status().isBadRequest());

        // Validate the VirementBancaire in the database
        List<VirementBancaire> virementBancaireList = virementBancaireRepository.findAll();
        assertThat(virementBancaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamVirementBancaire() throws Exception {
        int databaseSizeBeforeUpdate = virementBancaireRepository.findAll().size();
        virementBancaire.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restVirementBancaireMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(virementBancaire))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the VirementBancaire in the database
        List<VirementBancaire> virementBancaireList = virementBancaireRepository.findAll();
        assertThat(virementBancaireList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteVirementBancaire() throws Exception {
        // Initialize the database
        virementBancaireRepository.saveAndFlush(virementBancaire);

        int databaseSizeBeforeDelete = virementBancaireRepository.findAll().size();

        // Delete the virementBancaire
        restVirementBancaireMockMvc
            .perform(delete(ENTITY_API_URL_ID, virementBancaire.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<VirementBancaire> virementBancaireList = virementBancaireRepository.findAll();
        assertThat(virementBancaireList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
