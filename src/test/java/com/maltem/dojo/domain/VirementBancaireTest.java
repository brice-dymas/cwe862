package com.maltem.dojo.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.maltem.dojo.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class VirementBancaireTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VirementBancaire.class);
        VirementBancaire virementBancaire1 = new VirementBancaire();
        virementBancaire1.setId(1L);
        VirementBancaire virementBancaire2 = new VirementBancaire();
        virementBancaire2.setId(virementBancaire1.getId());
        assertThat(virementBancaire1).isEqualTo(virementBancaire2);
        virementBancaire2.setId(2L);
        assertThat(virementBancaire1).isNotEqualTo(virementBancaire2);
        virementBancaire1.setId(null);
        assertThat(virementBancaire1).isNotEqualTo(virementBancaire2);
    }
}
