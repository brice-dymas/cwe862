package com.maltem.dojo.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.maltem.dojo.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ClientBanqueTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientBanque.class);
        ClientBanque clientBanque1 = new ClientBanque();
        clientBanque1.setId(1L);
        ClientBanque clientBanque2 = new ClientBanque();
        clientBanque2.setId(clientBanque1.getId());
        assertThat(clientBanque1).isEqualTo(clientBanque2);
        clientBanque2.setId(2L);
        assertThat(clientBanque1).isNotEqualTo(clientBanque2);
        clientBanque1.setId(null);
        assertThat(clientBanque1).isNotEqualTo(clientBanque2);
    }
}
